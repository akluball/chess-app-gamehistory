# Chess Application Gamehistory

This is the gamehistory microservice of the [chess-application].

## Test

Integration tests require jdk8, docker.

Run test setup (start payara, database, and mock service containers and deploy application):
```
./gradlew iTestSetup
```
This only needs to be run before the first test run (not before each).

Run integration tests (redeploys application at start):
```
./gradlew iTest
```

Run test teardown (undeploys applications and mock services):
```
./gradlew iTestTeardown
```
This only needs to be run after the last test run (not after each).

## Build

```
./gradlew war
```

## Expected Environment

The application expects the following variables set in the deployment environment:
1. `CHESSAPP_USER_URI`
2. `CHESSAPP_GAMESTATE_URI`
3. `CHESSAPP_GAMEHISTORY_PRIVATEKEY`
4. `CHESSAPP_USER_PUBLICKEY`
5. `CHESSAPP_GAMESTATE_PUBLICKEY`
6. `CHESSAPP_MESSAGING_PUBLICKEY`

The application can be further configured with these optional environment variabels:
1. `CHESSAPP_GAMEHISTORY_SQL`
2. `CHESSAPP_GAMEHISTORY_ELOKFACTOR`
3. `CHESSAPP_GAMEHISTORY_MILLISPERPLAYER`

## Documentation

Create OpenAPI spec (written to `build/openapi.yaml`):
```
./gradlew resolve
```

## Notes

The test deployment uses postgres while the dev deployment uses embedded derby.

[chess-application]: https://gitlab.com/akluball/chess-app.git

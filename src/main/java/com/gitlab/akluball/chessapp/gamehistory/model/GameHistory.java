package com.gitlab.akluball.chessapp.gamehistory.model;

import com.gitlab.akluball.chessapp.gamehistory.exception.Http400;
import com.gitlab.akluball.chessapp.gamehistory.transfer.EndGameDto;
import com.gitlab.akluball.chessapp.gamehistory.transfer.GameResultDto;
import com.gitlab.akluball.chessapp.gamehistory.transfer.UserDto;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderColumn;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = GameHistory.Query.Name.FIND_BY_USER, query = GameHistory.Query.FIND_BY_USER),
        @NamedQuery(name = GameHistory.Query.Name.FIND_COMPLETED_BY_USER,
                query = GameHistory.Query.FIND_COMPLETED_BY_USER),
        @NamedQuery(name = GameHistory.Query.Name.AS_WHITE_SIDE_COUNT, query = GameHistory.Query.AS_WHITE_SIDE_COUNT),
        @NamedQuery(name = GameHistory.Query.Name.AS_BLACK_SIDE_COUNT, query = GameHistory.Query.AS_BLACK_SIDE_COUNT),
        @NamedQuery(name = GameHistory.Query.Name.VERSUS_COUNT, query = GameHistory.Query.VERSUS_COUNT),
})
public class GameHistory {

    @Id
    @GeneratedValue
    private int id;
    private int whiteSideUserId;
    private int blackSideUserId;
    private int whiteWinsRatingAdjustment;
    private int whiteLosesRatingAdjustment;
    private int whiteDrawsRatingAdjustment;
    private long startEpochSeconds;
    private long endEpochSeconds;
    @ElementCollection
    @OrderColumn
    private List<MoveSummary> moveSummaries = new ArrayList<>();
    private long millisPerPlayer;
    @Enumerated(EnumType.STRING)
    private ResultTag resultTag;
    @Enumerated(EnumType.STRING)
    private TerminationTag terminationTag;

    public int getId() {
        return this.id;
    }

    public boolean isParticipant(int userId) {
        return userId == this.whiteSideUserId || userId == this.blackSideUserId;
    }

    public void endGame(EndGameDto endGameDto) {
        if (this.resultTag != ResultTag.IN_PROGRESS) {
            throw new Http400(String.format("gamehistory not in progress: %s", this.id));
        }
        ResultTag resultTag = endGameDto.getResultTag();
        if (resultTag == ResultTag.IN_PROGRESS) {
            throw new Http400("cannot end game with in progress result tag");
        }
        TerminationTag terminationTag = endGameDto.getTerminationTag();
        if (terminationTag == TerminationTag.UNTERMINATED) {
            throw new Http400("cannot end game with unterminated termination tag");
        }
        this.resultTag = resultTag;
        this.terminationTag = terminationTag;
        this.endEpochSeconds = Instant.now().getEpochSecond();
        this.moveSummaries = endGameDto.getMoveSummaries();
    }

    public GameResultDto toGameResultDto() {
        int whiteSideRatingAdjustment = 0;
        int blackSideRatingAdjustment = 0;
        switch (this.resultTag) {
            case DRAW:
                whiteSideRatingAdjustment = this.whiteDrawsRatingAdjustment;
                blackSideRatingAdjustment = Math.negateExact(this.whiteDrawsRatingAdjustment);
                break;
            case WHITE_WINS:
                whiteSideRatingAdjustment = this.whiteWinsRatingAdjustment;
                blackSideRatingAdjustment = Math.negateExact(this.whiteWinsRatingAdjustment);
                break;
            case BLACK_WINS:
                whiteSideRatingAdjustment = this.whiteLosesRatingAdjustment;
                blackSideRatingAdjustment = Math.negateExact(this.whiteLosesRatingAdjustment);
                break;
        }
        return new GameResultDto.Builder()
                .whiteSideUserId(this.whiteSideUserId)
                .blackSideUserId(this.blackSideUserId)
                .whiteSideRatingAdjustment(whiteSideRatingAdjustment)
                .blackSideRatingAdjustment(blackSideRatingAdjustment)
                .build();
    }

    public GameStateCreateDto toGameStateCreateDto() {
        return this.new GameStateCreateDto();
    }

    public Dto toDto() {
        return this.new Dto();
    }

    public static class Builder {
        private final GameHistory gameHistory;
        private UserDto whiteSideUser;
        private UserDto blackSideUser;
        private int kFactor;

        public Builder() {
            this.gameHistory = new GameHistory();
            gameHistory.resultTag = ResultTag.IN_PROGRESS;
            gameHistory.terminationTag = TerminationTag.UNTERMINATED;
        }

        public Builder whiteSideUser(UserDto whiteSideUser) {
            this.whiteSideUser = whiteSideUser;
            return this;
        }

        public Builder blackSideUser(UserDto blackSideUser) {
            this.blackSideUser = blackSideUser;
            return this;
        }

        public Builder millisPerPlayer(Long millisPerPlayer) {
            this.gameHistory.millisPerPlayer = millisPerPlayer;
            return this;
        }

        public Builder eloRatingKFactor(int kFactor) {
            this.kFactor = kFactor;
            return this;
        }

        public GameHistory build() {
            gameHistory.whiteSideUserId = this.whiteSideUser.getId();
            gameHistory.blackSideUserId = this.blackSideUser.getId();
            EloRatingComputer eloRatingComputer
                    = new EloRatingComputer(this.whiteSideUser, this.blackSideUser, kFactor);
            gameHistory.whiteWinsRatingAdjustment = eloRatingComputer.getWhiteWinsRatingAdjustment();
            gameHistory.whiteLosesRatingAdjustment = eloRatingComputer.getWhiteLosesRatingAdjustment();
            gameHistory.whiteDrawsRatingAdjustment = eloRatingComputer.getWhiteDrawsRatingAdjustment();
            gameHistory.startEpochSeconds = Instant.now().getEpochSecond();
            return gameHistory;
        }
    }

    public static class Query {
        public static final String FIND_BY_USER
                = "SELECT gameHistory FROM GameHistory AS gameHistory"
                + " WHERE gameHistory.whiteSideUserId=:" + Param.USER_ID
                + " OR gameHistory.blackSideUserId=:" + Param.USER_ID
                + " ORDER BY gameHistory.startEpochSeconds DESC";
        public static final String FIND_COMPLETED_BY_USER
                = "SELECT gameHistory FROM GameHistory AS gameHistory"
                + " WHERE (gameHistory.whiteSideUserId=:" + Param.USER_ID
                + " OR gameHistory.blackSideUserId=:" + Param.USER_ID + ")"
                + " AND gameHistory.resultTag<>com.gitlab.akluball.chessapp.gamehistory.model.ResultTag.IN_PROGRESS"
                + " ORDER BY gameHistory.startEpochSeconds DESC";
        public static final String AS_WHITE_SIDE_COUNT
                = "SELECT COUNT(gameHistory) FROM GameHistory AS gameHistory"
                + " WHERE gameHistory.whiteSideUserId=:" + Param.USER_ID
                + " AND gameHistory.resultTag=:" + Param.RESULT_TAG;
        public static final String AS_BLACK_SIDE_COUNT
                = "SELECT COUNT(gameHistory) FROM GameHistory AS gameHistory"
                + " WHERE gameHistory.blackSideUserId=:" + Param.USER_ID
                + " AND gameHistory.resultTag=:" + Param.RESULT_TAG;
        public static final String VERSUS_COUNT
                = "SELECT COUNT(gameHistory) FROM GameHistory AS gameHistory"
                + " WHERE (gameHistory.whiteSideUserId=:" + Param.WHITE_SIDE_USER_ID
                + " AND gameHistory.blackSideUserId=:" + Param.BLACK_SIDE_USER_ID + ")"
                + " AND gameHistory.resultTag=:" + Param.RESULT_TAG;

        public static class Param {
            public static final String USER_ID = "userId";
            public static final String OPPONENT_USER_ID = "opponentUserId";
            public static final String RESULT_TAG = "resultTag";
            public static final String WHITE_SIDE_USER_ID = "whiteSideUserId";
            public static final String BLACK_SIDE_USER_ID = "blackSideUserId";
        }

        public static class Name {
            public static final String FIND_BY_USER = "findByUserId";
            public static final String FIND_COMPLETED_BY_USER = "findCompletedByUserId";
            public static final String AS_WHITE_SIDE_COUNT = "asWhiteSideCount";
            public static final String AS_BLACK_SIDE_COUNT = "asBlackSideCount";
            public static final String VERSUS_COUNT = "versusCount";
        }
    }

    @Schema(name = "GameHistory")
    public class Dto {
        private int id;
        private int whiteSideUserId;
        private int blackSideUserId;
        private long startEpochSeconds;
        private long endEpochSeconds;
        private long millisPerPlayer;
        private int whiteWinsRatingAdjustment;
        private int whiteLosesRatingAdjustment;
        private int whiteDrawsRatingAdjustment;
        private int moveCount;
        private List<MoveSummary.Dto> moveSummaries;
        private ResultTag resultTag;
        private TerminationTag terminationTag;

        private Dto() {
            GameHistory gameHistory = GameHistory.this;
            this.id = gameHistory.id;
            this.whiteSideUserId = gameHistory.whiteSideUserId;
            this.blackSideUserId = gameHistory.blackSideUserId;
            this.startEpochSeconds = gameHistory.startEpochSeconds;
            this.endEpochSeconds = gameHistory.endEpochSeconds;
            this.millisPerPlayer = gameHistory.millisPerPlayer;
            this.whiteWinsRatingAdjustment = gameHistory.whiteWinsRatingAdjustment;
            this.whiteLosesRatingAdjustment = gameHistory.whiteLosesRatingAdjustment;
            this.whiteDrawsRatingAdjustment = gameHistory.whiteDrawsRatingAdjustment;
            this.moveCount = gameHistory.moveSummaries.size();
            this.moveSummaries = gameHistory.moveSummaries.stream().map(MoveSummary::toDto)
                    .collect(Collectors.toList());
            this.resultTag = gameHistory.resultTag;
            this.terminationTag = gameHistory.terminationTag;
        }
    }

    public class GameStateCreateDto {
        private int whiteSideUserId;
        private int blackSideUserId;
        private long millisPerPlayer;

        private GameStateCreateDto() {
            GameHistory gameHistory = GameHistory.this;
            this.whiteSideUserId = gameHistory.whiteSideUserId;
            this.blackSideUserId = gameHistory.blackSideUserId;
            this.millisPerPlayer = gameHistory.millisPerPlayer;
        }
    }
}

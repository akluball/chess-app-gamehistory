package com.gitlab.akluball.chessapp.gamehistory.service;

import com.gitlab.akluball.chessapp.gamehistory.client.UserClient;
import com.gitlab.akluball.chessapp.gamehistory.data.GameRequestDao;
import com.gitlab.akluball.chessapp.gamehistory.exception.Http401;
import com.gitlab.akluball.chessapp.gamehistory.exception.Http403;
import com.gitlab.akluball.chessapp.gamehistory.exception.Http404;
import com.gitlab.akluball.chessapp.gamehistory.model.DirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.model.IndirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.transfer.UserDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Stateless
public class GameRequestService {

    private GameHistoryService gameHistoryService;
    private GameRequestDao gameRequestDao;
    private UserClient userClient;

    public GameRequestService() {
    }

    @Inject
    public GameRequestService(GameHistoryService gameHistoryService,
            GameRequestDao gameRequestDao,
            UserClient userClient) {
        this.gameHistoryService = gameHistoryService;
        this.gameRequestDao = gameRequestDao;
        this.userClient = userClient;
    }

    public void createDirect(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "requesteeId is required") Integer requesteeId
    ) {
        UserDto user = this.userClient.getUser(requesteeId);
        if (Objects.isNull(user)) {
            throw new Http404(String.format("User with id %s", requesteeId));
        }
        this.gameRequestDao.createDirect(currentUserId, requesteeId);
    }

    public List<DirectGameRequest> getIncomingDirect(@NotNull(payload = Http401.Payload.class) Integer currentUserId) {
        return this.gameRequestDao.getDirectRequestsTo(currentUserId);
    }

    public List<DirectGameRequest> getOutgoingDirect(@NotNull(payload = Http401.Payload.class) Integer currentUserId) {
        return this.gameRequestDao.getDirectRequestsFrom(currentUserId);
    }

    private DirectGameRequest getNonNullDirect(Integer requestId) {
        DirectGameRequest request = this.gameRequestDao.findDirectById(requestId);
        if (Objects.isNull(request)) {
            throw new Http404(String.format("DirectGameRequest with id %s", requestId));
        }
        return request;
    }

    public void declineDirect(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "requestId is required") Integer requestId) {
        DirectGameRequest request = getNonNullDirect(requestId);
        if (request.isNotRequestee(currentUserId)) {
            throw new Http403();
        }
        this.gameRequestDao.deleteDirect(request);
    }

    public GameHistory acceptDirect(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "requestId is required") Integer requestId
    ) {
        DirectGameRequest request = this.getNonNullDirect(requestId);
        if (request.isNotRequestee(currentUserId)) {
            throw new Http403();
        }
        UserDto requester = this.userClient.getUser(request.getRequesterId());
        if (Objects.isNull(requester)) {
            throw new Http404(String.format("User with id %s", request.getRequesterId()));
        }
        UserDto requestee = this.userClient.getUser(currentUserId);
        if (Objects.isNull(requestee)) {
            throw new Http404(String.format("User with id %s", request.getRequesteeId()));
        }
        GameHistory gameHistory = this.gameHistoryService.create(requestee, requester, request.getMillisPerPlayer());
        this.gameRequestDao.deleteDirect(request);
        return gameHistory;
    }

    public void cancelDirect(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "requestId is required") Integer requestId
    ) {
        DirectGameRequest requestToCancel = getNonNullDirect(requestId);
        if (!requestToCancel.isRequester(currentUserId)) {
            throw new Http403();
        }
        this.gameRequestDao.deleteDirect(requestToCancel);
    }

    public void createIndirect(@NotNull(payload = Http401.Payload.class) Integer currentUserId) {
        IndirectGameRequest indirectRequest = this.gameRequestDao.createDetachedIndirect(currentUserId);
        IndirectGameRequest fulfillingRequest = this.gameRequestDao.findFulfillingIndirectRequest(indirectRequest);
        if (Objects.nonNull(fulfillingRequest)) {
            UserDto requester = this.userClient.getUser(currentUserId);
            if (Objects.isNull(requester)) {
                throw new Http404(String.format("User with id %s", currentUserId));
            }
            UserDto fulfillingRequester = this.userClient.getUser(fulfillingRequest.getRequesterId());
            if (Objects.isNull(fulfillingRequester)) {
                throw new Http404(String.format("User with id %s", fulfillingRequest.getRequesterId()));
            }
            this.gameHistoryService.create(fulfillingRequester, requester, fulfillingRequest.getMillisPerPlayer());
            this.gameRequestDao.deleteIndirect(fulfillingRequest);
            this.gameRequestDao.deleteIndirect(indirectRequest);
        } else {
            this.gameRequestDao.persistDetachedIndirect(indirectRequest);
        }
    }

    public List<IndirectGameRequest> getOutgoingIndirect(@NotNull(payload = Http401.Payload.class) Integer currentUserId) {
        return this.gameRequestDao.getIndirectRequestsFrom(currentUserId);
    }

    public void cancelIndirect(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "requestId is required") Integer requestId
    ) {
        IndirectGameRequest requestToCancel = this.gameRequestDao.findIndirectById(requestId);
        if (Objects.isNull(requestToCancel)) {
            throw new Http404(String.format("IndirectRequest with id %s", requestId));
        }
        if (!requestToCancel.isRequester(currentUserId)) {
            throw new Http403();
        }
        this.gameRequestDao.deleteIndirect(requestToCancel);
    }
}

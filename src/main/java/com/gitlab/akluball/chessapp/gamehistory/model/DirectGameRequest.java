package com.gitlab.akluball.chessapp.gamehistory.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.time.Instant;

@Entity
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = DirectGameRequest.Query.Name.GET_INCOMING, query = DirectGameRequest.Query.GET_INCOMING),
        @NamedQuery(name = DirectGameRequest.Query.Name.GET_OUTGOING, query = DirectGameRequest.Query.GET_OUTGOING)
})
public class DirectGameRequest {
    @Id
    @GeneratedValue
    private Integer id;
    private Integer requesterId;
    private Integer requesteeId;
    private Long epochSeconds;
    private Long millisPerPlayer;

    public DirectGameRequest() {
    }

    public DirectGameRequest(Integer requesterId, Integer requesteeId, Long millisPerPlayer) {
        this.requesterId = requesterId;
        this.requesteeId = requesteeId;
        this.epochSeconds = Instant.now().getEpochSecond();
        this.millisPerPlayer = millisPerPlayer;
    }

    public Integer getRequesterId() {
        return this.requesterId;
    }

    public Integer getRequesteeId() {
        return this.requesteeId;
    }

    public Long getMillisPerPlayer() {
        return this.millisPerPlayer;
    }

    public boolean isRequester(Integer userId) {
        return this.requesterId.equals(userId);
    }

    public boolean isNotRequestee(Integer userId) {
        return !this.requesteeId.equals(userId);
    }

    public Dto toDto() {
        return this.new Dto();
    }

    public static class Query {
        public static final String GET_INCOMING
                = "SELECT request FROM DirectGameRequest AS request"
                + " WHERE request.requesteeId=:" + Param.REQUESTEE_ID
                + " ORDER BY request.epochSeconds DESC";
        public static final String GET_OUTGOING
                = "SELECT request FROM DirectGameRequest AS request"
                + " WHERE request.requesterId=:" + Param.REQUESTER_ID
                + " ORDER BY request.epochSeconds DESC";

        public static class Name {
            public static final String GET_INCOMING = "getIncomingRequests";
            public static final String GET_OUTGOING = "getOutgoingRequests";
        }

        public static class Param {
            public static final String REQUESTER_ID = "requesterId";
            public static final String REQUESTEE_ID = "requesteeId";
        }
    }

    @Schema(name = "DirectGameRequest")
    public class Dto {
        private Integer id;
        private Integer requesterId;
        private Integer requesteeId;
        private Long epochSeconds;

        private Dto() {
            DirectGameRequest directGameRequest = DirectGameRequest.this;
            this.id = directGameRequest.id;
            this.requesterId = directGameRequest.requesterId;
            this.requesteeId = directGameRequest.requesteeId;
            this.epochSeconds = directGameRequest.epochSeconds;
        }
    }
}

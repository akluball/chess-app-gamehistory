package com.gitlab.akluball.chessapp.gamehistory.data;

import com.gitlab.akluball.chessapp.gamehistory.GameHistoryConfig;
import com.gitlab.akluball.chessapp.gamehistory.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.model.ResultTag;
import com.gitlab.akluball.chessapp.gamehistory.transfer.UserDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class GameHistoryDao {

    @PersistenceContext(unitName = "gameHistoryPu")
    private EntityManager entityManager;
    @Inject
    private GameHistoryConfig gameHistoryConfig;

    public GameHistory create(UserDto whiteSideUser, UserDto blackSideUser, long millisPerPlayer) {
        GameHistory gameHistory = new GameHistory.Builder()
                .whiteSideUser(whiteSideUser)
                .blackSideUser(blackSideUser)
                .millisPerPlayer(millisPerPlayer)
                .eloRatingKFactor(this.gameHistoryConfig.eloRatingKFactor())
                .build();
        this.entityManager.persist(gameHistory);
        return gameHistory;
    }

    public GameHistory findById(int gameHistoryId) {
        return this.entityManager.find(GameHistory.class, gameHistoryId);
    }

    public List<GameHistory> findByUserId(int userId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.FIND_BY_USER, GameHistory.class)
                .setParameter(GameHistory.Query.Param.USER_ID, userId)
                .getResultList();
    }

    public List<GameHistory> findCompletedByUserId(int userId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.FIND_COMPLETED_BY_USER, GameHistory.class)
                .setParameter(GameHistory.Query.Param.USER_ID, userId)
                .getResultList();
    }

    public long getWinCountWhite(int userId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.AS_WHITE_SIDE_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.USER_ID, userId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.WHITE_WINS)
                .getSingleResult();
    }

    public long getLossCountWhite(int userId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.AS_WHITE_SIDE_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.USER_ID, userId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.BLACK_WINS)
                .getSingleResult();
    }

    public long getDrawCountWhite(int userId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.AS_WHITE_SIDE_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.USER_ID, userId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.DRAW)
                .getSingleResult();
    }

    public long getWinCountBlack(int userId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.AS_BLACK_SIDE_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.USER_ID, userId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.BLACK_WINS)
                .getSingleResult();
    }

    public long getLossCountBlack(int userId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.AS_BLACK_SIDE_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.USER_ID, userId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.WHITE_WINS)
                .getSingleResult();
    }

    public long getDrawCountBlack(int userId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.AS_BLACK_SIDE_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.USER_ID, userId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.DRAW)
                .getSingleResult();
    }

    public long getVersusWinCountWhite(int userId, int opponentUserId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.VERSUS_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.WHITE_SIDE_USER_ID, userId)
                .setParameter(GameHistory.Query.Param.BLACK_SIDE_USER_ID, opponentUserId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.WHITE_WINS)
                .getSingleResult();
    }

    public long getVersusLossCountWhite(int userId, int opponentUserId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.VERSUS_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.WHITE_SIDE_USER_ID, userId)
                .setParameter(GameHistory.Query.Param.BLACK_SIDE_USER_ID, opponentUserId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.BLACK_WINS)
                .getSingleResult();
    }

    public long getVersusDrawCountWhite(int userId, int opponentUserId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.VERSUS_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.WHITE_SIDE_USER_ID, userId)
                .setParameter(GameHistory.Query.Param.BLACK_SIDE_USER_ID, opponentUserId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.DRAW)
                .getSingleResult();
    }

    public long getVersusWinCountBlack(int userId, int opponentUserId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.VERSUS_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.BLACK_SIDE_USER_ID, userId)
                .setParameter(GameHistory.Query.Param.WHITE_SIDE_USER_ID, opponentUserId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.BLACK_WINS)
                .getSingleResult();
    }

    public long getVersusLossCountBlack(int userId, int opponentUserId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.VERSUS_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.BLACK_SIDE_USER_ID, userId)
                .setParameter(GameHistory.Query.Param.WHITE_SIDE_USER_ID, opponentUserId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.WHITE_WINS)
                .getSingleResult();
    }

    public long getVersusDrawCountBlack(int userId, int opponentUserId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.VERSUS_COUNT, Long.class)
                .setParameter(GameHistory.Query.Param.BLACK_SIDE_USER_ID, userId)
                .setParameter(GameHistory.Query.Param.WHITE_SIDE_USER_ID, opponentUserId)
                .setParameter(GameHistory.Query.Param.RESULT_TAG, ResultTag.DRAW)
                .getSingleResult();
    }
}

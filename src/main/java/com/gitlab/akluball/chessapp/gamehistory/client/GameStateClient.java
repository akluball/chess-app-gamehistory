package com.gitlab.akluball.chessapp.gamehistory.client;

import com.gitlab.akluball.chessapp.gamehistory.GameHistoryConfig;
import com.gitlab.akluball.chessapp.gamehistory.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.security.GameHistorySecurity;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.bind.Jsonb;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static com.gitlab.akluball.chessapp.gamehistory.util.Util.bearer;
import static com.gitlab.akluball.chessapp.gamehistory.util.Util.is2xx;

@Singleton
public class GameStateClient {
    private final WebTarget target;
    private final Jsonb jsonb;
    private final GameHistorySecurity gameHistorySecurity;

    @Inject
    GameStateClient(GameHistoryConfig gameHistoryConfig,
            Client client,
            Jsonb jsonb,
            GameHistorySecurity gameHistorySecurity) {
        this.target = client.target(gameHistoryConfig.gameStateUri());
        this.jsonb = jsonb;
        this.gameHistorySecurity = gameHistorySecurity;
    }

    public boolean createGameState(GameHistory gameHistory) {
        String serialized = this.jsonb.toJson(gameHistory.toGameStateCreateDto());
        Response response = this.target.path("api")
                .path("gamestate")
                .path("" + gameHistory.getId())
                .request()
                .header("Authorization", bearer(this.gameHistorySecurity.getToken()))
                .put(Entity.json(serialized));
        return is2xx(response);
    }
}

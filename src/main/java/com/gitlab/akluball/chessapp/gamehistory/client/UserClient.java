package com.gitlab.akluball.chessapp.gamehistory.client;

import com.gitlab.akluball.chessapp.gamehistory.GameHistoryConfig;
import com.gitlab.akluball.chessapp.gamehistory.security.GameHistorySecurity;
import com.gitlab.akluball.chessapp.gamehistory.transfer.GameResultDto;
import com.gitlab.akluball.chessapp.gamehistory.transfer.UserDto;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static com.gitlab.akluball.chessapp.gamehistory.util.Util.bearer;
import static com.gitlab.akluball.chessapp.gamehistory.util.Util.is2xx;

@Singleton
public class UserClient {
    private WebTarget userTarget;
    private GameHistorySecurity gameHistorySecurity;

    @Inject
    UserClient(GameHistoryConfig gameHistoryConfig, Client client, GameHistorySecurity gameHistorySecurity) {
        this.userTarget = client.target(gameHistoryConfig.userUri())
                .path("api")
                .path("user");
        this.gameHistorySecurity = gameHistorySecurity;
    }

    public UserDto getUser(int userId) {
        Response response = this.userTarget.path("" + userId)
                .request()
                .header("Authorization", bearer(this.gameHistorySecurity.getToken()))
                .get();
        return is2xx(response) ? response.readEntity(UserDto.class) : null;
    }

    public boolean pumpGameResult(GameResultDto gameResultDto) {
        Response response = this.userTarget.path("endgame")
                .request()
                .header("Authorization", bearer(this.gameHistorySecurity.getToken()))
                .post(Entity.json(gameResultDto));
        return is2xx(response);
    }
}

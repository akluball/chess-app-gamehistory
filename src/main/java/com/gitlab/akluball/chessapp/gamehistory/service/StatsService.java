package com.gitlab.akluball.chessapp.gamehistory.service;

import com.gitlab.akluball.chessapp.gamehistory.data.GameHistoryDao;
import com.gitlab.akluball.chessapp.gamehistory.transfer.BySideStatsDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

@Stateless
public class StatsService {
    private GameHistoryDao gameHistoryDao;

    public StatsService() {
    }

    @Inject
    public StatsService(GameHistoryDao gameHistoryDao) {
        this.gameHistoryDao = gameHistoryDao;
    }

    public BySideStatsDto getUserStats(@NotNull(message = "userId is required") Integer userId) {
        return new BySideStatsDto.Builder()
                .whiteSideWinCount(this.gameHistoryDao.getWinCountWhite(userId))
                .whiteSideLossCount(this.gameHistoryDao.getLossCountWhite(userId))
                .whiteSideDrawCount(this.gameHistoryDao.getDrawCountWhite(userId))
                .blackSideWinCount(this.gameHistoryDao.getWinCountBlack(userId))
                .blackSideLossCount(this.gameHistoryDao.getLossCountBlack(userId))
                .blackSideDrawCount(this.gameHistoryDao.getDrawCountBlack(userId))
                .build();
    }

    public BySideStatsDto getVersusStats(
            @NotNull(message = "userId is required") Integer userId,
            @NotNull(message = "opponentUserId is required") Integer opponentUserId
    ) {
        return new BySideStatsDto.Builder()
                .whiteSideWinCount(this.gameHistoryDao.getVersusWinCountWhite(userId, opponentUserId))
                .whiteSideLossCount(this.gameHistoryDao.getVersusLossCountWhite(userId, opponentUserId))
                .whiteSideDrawCount(this.gameHistoryDao.getVersusDrawCountWhite(userId, opponentUserId))
                .blackSideWinCount(this.gameHistoryDao.getVersusWinCountBlack(userId, opponentUserId))
                .blackSideLossCount(this.gameHistoryDao.getVersusLossCountBlack(userId, opponentUserId))
                .blackSideDrawCount(this.gameHistoryDao.getVersusDrawCountBlack(userId, opponentUserId))
                .build();
    }
}

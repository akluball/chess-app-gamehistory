package com.gitlab.akluball.chessapp.gamehistory.http;

import javax.ws.rs.ext.Provider;

@Provider
public class IntegerTypeCheckParamConverterProvider extends TypeCheckParamConverterProvider<Integer> {
    public IntegerTypeCheckParamConverterProvider() {
        super(Integer.class);
    }

    @Override
    protected boolean isValidType(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

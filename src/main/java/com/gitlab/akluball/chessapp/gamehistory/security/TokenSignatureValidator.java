package com.gitlab.akluball.chessapp.gamehistory.security;

import com.nimbusds.jwt.SignedJWT;

public interface TokenSignatureValidator {
    boolean isValidSignature(SignedJWT token);
}

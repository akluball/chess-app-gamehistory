package com.gitlab.akluball.chessapp.gamehistory.model;

import com.gitlab.akluball.chessapp.gamehistory.transfer.UserDto;

public class EloRatingComputer {
    private final int whiteWinsRatingAdjustment;
    private final int whiteLosesRatingAdjustment;
    private final int whiteDrawsRatingAdjustment;

    public EloRatingComputer(UserDto whiteSideUser, UserDto blackSideUser, int kFactor) {
        double whiteSideExpectedScore
                = 1.0 / (1.0 + Math.pow(10.0, (blackSideUser.getRating() - whiteSideUser.getRating()) / 400.0));
        this.whiteWinsRatingAdjustment = (int) Math.floor(kFactor * (1.0 - whiteSideExpectedScore));
        this.whiteLosesRatingAdjustment = (int) Math.floor(kFactor * -1 * whiteSideExpectedScore);
        this.whiteDrawsRatingAdjustment = (int) Math.floor(kFactor * (0.5 - whiteSideExpectedScore));
    }

    public int getWhiteWinsRatingAdjustment() {
        return this.whiteWinsRatingAdjustment;
    }

    public int getWhiteLosesRatingAdjustment() {
        return this.whiteLosesRatingAdjustment;
    }

    public int getWhiteDrawsRatingAdjustment() {
        return this.whiteDrawsRatingAdjustment;
    }
}

package com.gitlab.akluball.chessapp.gamehistory.controller;

import com.gitlab.akluball.chessapp.gamehistory.controller.openapi.OpenApiSecuritySchemes;
import com.gitlab.akluball.chessapp.gamehistory.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.gamehistory.model.DirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.security.Bearer;
import com.gitlab.akluball.chessapp.gamehistory.security.BearerRole;
import com.gitlab.akluball.chessapp.gamehistory.security.CurrentUserId;
import com.gitlab.akluball.chessapp.gamehistory.service.GameRequestService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("gamerequest/direct")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.DIRECT_GAME_REQUEST)
@SecurityRequirement(name = OpenApiSecuritySchemes.BEARER)
public class DirectGameRequestController {

    private GameRequestService gameRequestService;
    private Integer currentUserId;

    public DirectGameRequestController() {
    }

    @Inject
    public DirectGameRequestController(GameRequestService gameRequestService, @CurrentUserId Integer currentUserId) {
        this.gameRequestService = gameRequestService;
        this.currentUserId = currentUserId;
    }

    @POST
    @Bearer(roles = { BearerRole.USER })
    @Operation(
            summary = "Creates DirectGameRequest",
            description = "Creates DirectGameRequest from user to given requesteeId"
    )
    @ApiResponse(responseCode = "204", description = "DirectGameRequest created")
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "404", description = "not found")
    public void createDirect(@QueryParam("to") Integer requesteeId) {
        this.gameRequestService.createDirect(this.currentUserId, requesteeId);
    }

    @GET
    @Path("incoming")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets incoming DirectGameRequests", description = "Gets DirectGameRequests to user")
    @ApiResponse(
            responseCode = "200",
            description = "Incoming DirectGameRequests",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = DirectGameRequest.Dto.class)))
    )
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public List<DirectGameRequest.Dto> getIncomingDirect() {
        return this.gameRequestService.getIncomingDirect(this.currentUserId)
                .stream()
                .map(DirectGameRequest::toDto)
                .collect(Collectors.toList());
    }

    @GET
    @Path("outgoing")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets outgoing DirectGameRequests", description = "Gets DirectGameRequests from user")
    @ApiResponse(
            responseCode = "200",
            description = "Outgoing DirectGameRequests",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = DirectGameRequest.Dto.class)))
    )
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public List<DirectGameRequest.Dto> getOutgoingDirect() {
        return this.gameRequestService.getOutgoingDirect(this.currentUserId)
                .stream()
                .map(DirectGameRequest::toDto)
                .collect(Collectors.toList());
    }

    @POST
    @Path("{directRequestId}/decline")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Declines DirectGameRequest", description = "Declines DirectGameRequest to user")
    @ApiResponse(responseCode = "204", description = "DirectGameRequest declined")
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    public void declineDirect(@PathParam("directRequestId") Integer requestId) {
        this.gameRequestService.declineDirect(this.currentUserId, requestId);
    }

    @POST
    @Path("{directRequestId}/accept")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Accepts DirectGameRequest", description = "Accepts DirectGameRequest to user")
    @ApiResponse(
            responseCode = "200",
            description = "Created GameHistory",
            content = @Content(schema = @Schema(implementation = GameHistory.Dto.class))
    )
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    @ApiResponse(responseCode = "503", description = "unable to create GameState")
    public GameHistory.Dto acceptDirect(@PathParam("directRequestId") Integer requestId) {
        return this.gameRequestService.acceptDirect(this.currentUserId, requestId).toDto();
    }

    @DELETE
    @Path("{directRequestId}")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Cancels DirectGameRequest", description = "Cancels DirectGameRequest from user")
    @ApiResponse(responseCode = "204", description = "DirectGameRequest cancelled")
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    public void cancelDirect(@PathParam("directRequestId") Integer requestId) {
        this.gameRequestService.cancelDirect(this.currentUserId, requestId);
    }
}

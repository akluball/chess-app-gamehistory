package com.gitlab.akluball.chessapp.gamehistory.controller;

import com.gitlab.akluball.chessapp.gamehistory.controller.openapi.OpenApiSecuritySchemes;
import com.gitlab.akluball.chessapp.gamehistory.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.gamehistory.security.Bearer;
import com.gitlab.akluball.chessapp.gamehistory.security.BearerRole;
import com.gitlab.akluball.chessapp.gamehistory.security.CurrentUserId;
import com.gitlab.akluball.chessapp.gamehistory.service.StatsService;
import com.gitlab.akluball.chessapp.gamehistory.transfer.BySideStatsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("stats")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.STATS)
@SecurityRequirement(name = OpenApiSecuritySchemes.BEARER)
public class StatsController {
    private Integer currentUserId;
    private StatsService statsService;

    public StatsController() {
    }

    @Inject
    public StatsController(StatsService statsService, @CurrentUserId Integer currentUserId) {
        this.statsService = statsService;
        this.currentUserId = currentUserId;
    }

    @GET
    @Path("user/{userId}")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets user stats", description = "Gets stats for given user id")
    @ApiResponse(
            responseCode = "200",
            description = "BySideStats for given user",
            content = @Content(schema = @Schema(implementation = BySideStatsDto.class))
    )
    @ApiResponse(responseCode = "400", description = "not found")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public BySideStatsDto getUserStats(@PathParam("userId") Integer userId) {
        return this.statsService.getUserStats(userId);
    }

    @GET
    @Path("versus/{opponentUserId}")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets versus stats", description = "Gets versus stats between user and give opponent")
    @ApiResponse(
            responseCode = "200",
            description = "BySideStats for user vs. given opponent",
            content = @Content(schema = @Schema(implementation = BySideStatsDto.class))
    )
    @ApiResponse(responseCode = "400", description = "not found")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public BySideStatsDto getVersusStats(@PathParam("opponentUserId") Integer opponentUserId) {
        return this.statsService.getVersusStats(this.currentUserId, opponentUserId);
    }
}

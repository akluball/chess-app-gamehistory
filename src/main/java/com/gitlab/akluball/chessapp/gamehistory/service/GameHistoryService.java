package com.gitlab.akluball.chessapp.gamehistory.service;

import com.gitlab.akluball.chessapp.gamehistory.client.GameStateClient;
import com.gitlab.akluball.chessapp.gamehistory.client.UserClient;
import com.gitlab.akluball.chessapp.gamehistory.data.GameHistoryDao;
import com.gitlab.akluball.chessapp.gamehistory.exception.Http401;
import com.gitlab.akluball.chessapp.gamehistory.exception.Http403;
import com.gitlab.akluball.chessapp.gamehistory.exception.Http404;
import com.gitlab.akluball.chessapp.gamehistory.exception.Http503;
import com.gitlab.akluball.chessapp.gamehistory.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.transfer.EndGameDto;
import com.gitlab.akluball.chessapp.gamehistory.transfer.GameResultDto;
import com.gitlab.akluball.chessapp.gamehistory.transfer.UserDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Objects;

@Stateless
public class GameHistoryService {

    private GameHistoryDao gameHistoryDao;
    private UserClient userClient;
    private GameStateClient gameStateClient;

    public GameHistoryService() {
    }

    @Inject
    public GameHistoryService(GameHistoryDao gameHistoryDao, UserClient userClient, GameStateClient gameStateClient) {
        this.gameHistoryDao = gameHistoryDao;
        this.userClient = userClient;
        this.gameStateClient = gameStateClient;
    }

    public GameHistory create(
            @NotNull(message = "User is required") @Valid UserDto whiteSideUser,
            @NotNull(message = "User is required") @Valid UserDto blackSideUser,
            @NotNull(message = "millisPerPlayer is required") @Positive(message = "millisPerPlayer must be positive") Long millisPerPlayer
    ) {
        GameHistory gameHistory = this.gameHistoryDao.create(whiteSideUser, blackSideUser, millisPerPlayer);
        if (!this.gameStateClient.createGameState(gameHistory)) {
            throw new Http503("unable to create GameState");
        }
        return gameHistory;
    }

    public GameHistory getGameHistory(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "gameHistoryId is required") Integer gameHistoryId
    ) {
        GameHistory gameHistory = this.getGameHistory(gameHistoryId);
        if (!gameHistory.isParticipant(currentUserId)) {
            throw new Http403();
        }
        return gameHistory;
    }

    public GameHistory getGameHistory(@NotNull(message = "gameHistoryId is required") Integer gameHistoryId) {
        GameHistory found = this.gameHistoryDao.findById(gameHistoryId);
        if (Objects.isNull(found)) {
            throw new Http404(String.format("GameHistory with id %s", gameHistoryId));
        }
        return found;
    }

    public List<GameHistory> getByParticipant(@NotNull(message = "userId is required") Integer userId) {
        return this.gameHistoryDao.findByUserId(userId);
    }

    public List<GameHistory> getCompletedByParticipant(@NotNull(message = "userId is required") Integer userId) {
        return this.gameHistoryDao.findCompletedByUserId(userId);
    }

    public void endGame(
            @NotNull(message = "gameHistoryId is required") Integer gameHistoryId,
            @NotNull(message = "EndGame is required") @Valid EndGameDto endGameDto
    ) {
        GameHistory gameHistory = this.gameHistoryDao.findById(gameHistoryId);
        if (Objects.isNull(gameHistory)) {
            throw new Http404(String.format("GameHistory with id %s", gameHistoryId));
        }
        gameHistory.endGame(endGameDto);
        GameResultDto gameResultDto = gameHistory.toGameResultDto();
        if (!this.userClient.pumpGameResult(gameResultDto)) {
            throw new Http503("unable to pump end game result to user service");
        }
    }
}

package com.gitlab.akluball.chessapp.gamehistory.exception;

public class Http503 extends HttpException {
    public Http503(String description) {
        super(503, String.format("service unavailable: %s", description));
    }
}

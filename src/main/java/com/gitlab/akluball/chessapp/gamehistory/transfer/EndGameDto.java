package com.gitlab.akluball.chessapp.gamehistory.transfer;

import com.gitlab.akluball.chessapp.gamehistory.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamehistory.model.ResultTag;
import com.gitlab.akluball.chessapp.gamehistory.model.TerminationTag;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Schema(name = "EndGame")
public class EndGameDto {
    @NotNull(message = "EndGame.resultTag is required")
    private ResultTag resultTag;
    @NotNull(message = "EndGame.terminationTag is required")
    private TerminationTag terminationTag;
    @NotNull(message = "EndGame.moveSummaries is required")
    private List<MoveSummary.Dto> moveSummaries;

    public ResultTag getResultTag() {
        return resultTag;
    }

    public TerminationTag getTerminationTag() {
        return terminationTag;
    }

    public List<MoveSummary> getMoveSummaries() {
        return moveSummaries.stream()
                .map(MoveSummary.Dto::toMoveSummary)
                .collect(Collectors.toList());
    }
}

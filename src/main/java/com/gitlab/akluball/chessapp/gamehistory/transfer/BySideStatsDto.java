package com.gitlab.akluball.chessapp.gamehistory.transfer;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "BySideStats")
public class BySideStatsDto {
    private StatsDto whiteSideStats;
    private StatsDto blackSideStats;

    private BySideStatsDto(StatsDto whiteSideStats, StatsDto blackSideStats) {
        this.whiteSideStats = whiteSideStats;
        this.blackSideStats = blackSideStats;
    }

    @Schema(name = "Stats")
    public static class StatsDto {
        private long winCount;
        private long lossCount;
        private long drawCount;
    }

    public static class Builder {
        private final StatsDto whiteSideStatsDto;
        private final StatsDto blackSideStatsDto;

        public Builder() {
            this.whiteSideStatsDto = new StatsDto();
            this.blackSideStatsDto = new StatsDto();
        }

        public Builder whiteSideWinCount(long winCount) {
            this.whiteSideStatsDto.winCount = winCount;
            return this;
        }

        public Builder whiteSideLossCount(long lossCount) {
            this.whiteSideStatsDto.lossCount = lossCount;
            return this;
        }

        public Builder whiteSideDrawCount(long drawCount) {
            this.whiteSideStatsDto.drawCount = drawCount;
            return this;
        }

        public Builder blackSideWinCount(long winCount) {
            this.blackSideStatsDto.winCount = winCount;
            return this;
        }

        public Builder blackSideLossCount(long lossCount) {
            this.blackSideStatsDto.lossCount = lossCount;
            return this;
        }

        public Builder blackSideDrawCount(long drawCount) {
            this.blackSideStatsDto.drawCount = drawCount;
            return this;
        }

        public BySideStatsDto build() {
            BySideStatsDto bySideStatsDto = new BySideStatsDto(this.whiteSideStatsDto, this.blackSideStatsDto);
            return bySideStatsDto;
        }
    }
}

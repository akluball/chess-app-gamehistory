package com.gitlab.akluball.chessapp.gamehistory.security;

import com.gitlab.akluball.chessapp.gamehistory.GameHistoryConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.gitlab.akluball.chessapp.gamehistory.util.Util.asRuntime;

@Singleton
public class GameHistorySecurity {
    private final RSASSASigner signer;
    private final int secondsToExpiration;
    private SignedJWT token;
    private Instant expiration;

    @Inject
    GameHistorySecurity(GameHistoryConfig gameHistoryConfig) {
        this.signer = new RSASSASigner(gameHistoryConfig.gameHistoryPrivateKey());
        this.secondsToExpiration = gameHistoryConfig.tokenLifeSeconds();
    }

    private boolean requiresRefresh() {
        return Objects.isNull(this.token) || this.expiration.isBefore(Instant.now().plusSeconds(5));
    }

    private void refresh() {
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        this.expiration = Instant.now().plusSeconds(this.secondsToExpiration);
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject("" + ServiceName.GAMEHISTORY.asString())
                .claim(BearerRole.CLAIM_NAME, Stream.of(BearerRole.SERVICE.asString()).collect(Collectors.toList()))
                .expirationTime(Date.from(this.expiration))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        try {
            token.sign(this.signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        this.token = token;
    }

    public String getToken() {
        if (this.requiresRefresh()) {
            this.refresh();
        }
        return this.token.serialize();
    }
}

package com.gitlab.akluball.chessapp.gamehistory.controller;

import com.gitlab.akluball.chessapp.gamehistory.controller.openapi.OpenApiSecuritySchemes;
import com.gitlab.akluball.chessapp.gamehistory.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.gamehistory.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.security.Bearer;
import com.gitlab.akluball.chessapp.gamehistory.security.BearerRole;
import com.gitlab.akluball.chessapp.gamehistory.security.CurrentUserId;
import com.gitlab.akluball.chessapp.gamehistory.security.ServiceName;
import com.gitlab.akluball.chessapp.gamehistory.service.GameHistoryService;
import com.gitlab.akluball.chessapp.gamehistory.transfer.EndGameDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Path("gamehistory")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.GAMEHISTORY)
@SecurityRequirement(name = OpenApiSecuritySchemes.BEARER)
public class GameHistoryController {

    private GameHistoryService gameHistoryService;
    private Integer currentUserId;
    private BearerRole bearerRole;

    public GameHistoryController() {
    }

    @Inject
    public GameHistoryController(
            GameHistoryService gameHistoryService,
            @CurrentUserId Integer currentUserId,
            BearerRole bearerRole
    ) {
        this.gameHistoryService = gameHistoryService;
        this.currentUserId = currentUserId;
        this.bearerRole = bearerRole;
    }

    @GET
    @Path("{gameHistoryId}")
    @Bearer(roles = { BearerRole.USER, BearerRole.SERVICE }, serviceNames = { ServiceName.MESSAGING })
    @Operation(summary = "Gets GameHistory by id", description = "Gets GameHistory with the given id")
    @ApiResponse(
            responseCode = "200",
            description = "GameHistory with given id",
            content = @Content(schema = @Schema(implementation = GameHistory.Dto.class))
    )
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    public GameHistory.Dto getById(@PathParam("gameHistoryId") Integer gameHistoryId) {
        if (BearerRole.USER.equals(this.bearerRole)) {
            return this.gameHistoryService.getGameHistory(this.currentUserId, gameHistoryId).toDto();
        } else {
            return this.gameHistoryService.getGameHistory(gameHistoryId).toDto();
        }
    }

    @GET
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets GameHistories for user", description = "Gets GameHistories with user as participant")
    @ApiResponse(
            responseCode = "200",
            description = "GameHistories with user as participant",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = GameHistory.Dto.class)))
    )
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public List<GameHistory.Dto> getByParticipant(
            @QueryParam("userId") Integer userId,
            @DefaultValue("false") @QueryParam("isCompleted") Boolean isCompleted
    ) {
        userId = (Objects.nonNull(userId)) ? userId : this.currentUserId;
        List<GameHistory> gameHistories = (isCompleted)
                ? this.gameHistoryService.getCompletedByParticipant(userId)
                : this.gameHistoryService.getByParticipant(userId);
        return gameHistories.stream()
                .map(GameHistory::toDto)
                .collect(Collectors.toList());
    }

    @POST
    @Path("{gameHistoryId}/end")
    @Bearer(roles = { BearerRole.SERVICE }, serviceNames = { ServiceName.GAMESTATE })
    @Operation(
            summary = "Performs end operation",
            description = "Performs end operation on GameHistory with given id"
    )
    @ApiResponse(responseCode = "204", description = "End operation performed")
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "404", description = "not found")
    @ApiResponse(responseCode = "503", description = "unable to inform user service")
    public void endGame(@PathParam("gameHistoryId") Integer gameHistoryId, EndGameDto endGameDto) {
        this.gameHistoryService.endGame(gameHistoryId, endGameDto);
    }
}

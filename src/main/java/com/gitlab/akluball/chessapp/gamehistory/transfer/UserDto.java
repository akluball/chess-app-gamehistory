package com.gitlab.akluball.chessapp.gamehistory.transfer;

import javax.validation.constraints.NotNull;

public class UserDto {
    @NotNull(message = "User.id is required")
    private Integer id;
    @NotNull(message = "User.rating is required")
    private Integer rating;

    public Integer getId() {
        return this.id;
    }

    public Integer getRating() {
        return this.rating;
    }
}

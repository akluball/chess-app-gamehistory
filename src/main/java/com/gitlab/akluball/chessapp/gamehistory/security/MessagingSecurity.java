package com.gitlab.akluball.chessapp.gamehistory.security;

import com.gitlab.akluball.chessapp.gamehistory.GameHistoryConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MessagingSecurity implements TokenSignatureValidator {
    private RSASSAVerifier verifier;

    @Inject
    MessagingSecurity(GameHistoryConfig gameHistoryConfig) {
        this.verifier = new RSASSAVerifier(gameHistoryConfig.messagingPublicKey());
    }

    @Override
    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }
}

package com.gitlab.akluball.chessapp.gamehistory.controller;

import com.gitlab.akluball.chessapp.gamehistory.controller.openapi.OpenApiSecuritySchemes;
import com.gitlab.akluball.chessapp.gamehistory.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.gamehistory.model.IndirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.security.Bearer;
import com.gitlab.akluball.chessapp.gamehistory.security.BearerRole;
import com.gitlab.akluball.chessapp.gamehistory.security.CurrentUserId;
import com.gitlab.akluball.chessapp.gamehistory.service.GameRequestService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("gamerequest/indirect")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.INDIRECT_GAME_REQUEST)
@SecurityRequirement(name = OpenApiSecuritySchemes.BEARER)
public class IndirectGameRequestController {
    private GameRequestService gameRequestService;
    private Integer currentUserId;

    public IndirectGameRequestController() {
    }

    @Inject
    public IndirectGameRequestController(GameRequestService gameRequestService, @CurrentUserId Integer currentUserId) {
        this.gameRequestService = gameRequestService;
        this.currentUserId = currentUserId;
    }

    @POST
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Creates IndirectGameRequest", description = "Creates IndirectGameRequest from user")
    @ApiResponse(responseCode = "204", description = "IndirectGameRequest created")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public void createIndirect() {
        this.gameRequestService.createIndirect(this.currentUserId);
    }

    @GET
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets outgoing IndirectGameRequests", description = "Gets IndirectGameRequests from user")
    @ApiResponse(
            responseCode = "200",
            description = "IndirectGameRequests from user",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = IndirectGameRequest.Dto.class)))
    )
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public List<IndirectGameRequest.Dto> getOutgoingIndirect() {
        return this.gameRequestService.getOutgoingIndirect(this.currentUserId)
                .stream()
                .map(IndirectGameRequest::toDto)
                .collect(Collectors.toList());
    }

    @DELETE
    @Path("{indirectRequestId}")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Cancels IndirectGameRequest", description = "Cancels IndirectGameRequest from user")
    @ApiResponse(responseCode = "204", description = "IndirectGameRequest cancelled")
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    public void cancelIndirect(@PathParam("indirectRequestId") Integer indirectRequestId) {
        this.gameRequestService.cancelIndirect(this.currentUserId, indirectRequestId);
    }
}

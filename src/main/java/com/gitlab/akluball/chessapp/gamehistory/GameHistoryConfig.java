package com.gitlab.akluball.chessapp.gamehistory;

import javax.inject.Singleton;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.gamehistory.util.Util.minutesToMillis;
import static com.gitlab.akluball.chessapp.gamehistory.util.Util.minutesToSeconds;

@Singleton
public class GameHistoryConfig {
    private final String userUri;
    private final String gameStateUri;
    private final PrivateKey gameHistoryPrivateKey;
    private final RSAPublicKey userPublicKey;
    private final RSAPublicKey gameStatePublicKey;
    private final RSAPublicKey messagingPublicKey;
    private final int eloRatingKFactor;
    private final long defaultMillisPerPlayer;

    GameHistoryConfig() throws NoSuchAlgorithmException, InvalidKeySpecException {
        this.userUri = System.getenv("CHESSAPP_USER_URI");
        this.gameStateUri = System.getenv("CHESSAPP_GAMESTATE_URI");
        Base64.Decoder decoder = Base64.getDecoder();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        byte[] gameHistoryPrivateKeyBytes = decoder.decode(System.getenv("CHESSAPP_GAMEHISTORY_PRIVATEKEY"));
        this.gameHistoryPrivateKey = keyFactory
                .generatePrivate(new PKCS8EncodedKeySpec(gameHistoryPrivateKeyBytes));
        byte[] userPublicKeyBytes = decoder.decode(System.getenv("CHESSAPP_USER_PUBLICKEY"));
        this.userPublicKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(userPublicKeyBytes));
        byte[] gameStatePublicKeyBytes = decoder.decode(System.getenv("CHESSAPP_GAMESTATE_PUBLICKEY"));
        this.gameStatePublicKey = (RSAPublicKey) keyFactory
                .generatePublic(new X509EncodedKeySpec(gameStatePublicKeyBytes));
        byte[] messagingPublicKeyBytes = decoder.decode(System.getenv("CHESSAPP_MESSAGING_PUBLICKEY"));
        this.messagingPublicKey = (RSAPublicKey) keyFactory
                .generatePublic(new X509EncodedKeySpec(messagingPublicKeyBytes));
        this.eloRatingKFactor = fromEnvOrDefault("CHESSAPP_GAMEHISTORY_ELOKFACTOR", 32);
        this.defaultMillisPerPlayer = fromEnvOrDefault("CHESSAPP_GAMEHISTORY_MILLISPERPLAYER", minutesToMillis(10));
    }

    private static int fromEnvOrDefault(String env, int defaultTo) {
        String fromEnv = System.getenv(env);
        return (Objects.nonNull(fromEnv)) ? Integer.parseInt(fromEnv) : defaultTo;
    }

    private static long fromEnvOrDefault(String env, long defaultTo) {
        String fromEnv = System.getenv(env);
        return (Objects.nonNull(fromEnv)) ? Long.parseLong(fromEnv) : defaultTo;
    }

    public String userUri() {
        return this.userUri;
    }

    public String gameStateUri() {
        return this.gameStateUri;
    }

    public PrivateKey gameHistoryPrivateKey() {
        return this.gameHistoryPrivateKey;
    }

    public int tokenLifeSeconds() {
        return minutesToSeconds(5);
    }

    public RSAPublicKey userPublicKey() {
        return this.userPublicKey;
    }

    public RSAPublicKey gameStatePublicKey() {
        return this.gameStatePublicKey;
    }

    public RSAPublicKey messagingPublicKey() {
        return this.messagingPublicKey;
    }

    public int eloRatingKFactor() {
        return this.eloRatingKFactor;
    }

    public long defaultMillisPerPlayer() {
        return this.defaultMillisPerPlayer;
    }
}

package com.gitlab.akluball.chessapp.gamehistory.security;

import com.nimbusds.jwt.SignedJWT;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

public class SecurityUtil {
    public static boolean isExpired(SignedJWT token) {
        try {
            Date expiration = token.getJWTClaimsSet().getExpirationTime();
            return Objects.isNull(expiration) || !expiration.toInstant().isAfter(Instant.now());
        } catch (ParseException e) {
            return true;
        }
    }

    public static boolean isNotExpired(SignedJWT token) {
        return !isExpired(token);
    }
}

package com.gitlab.akluball.chessapp.gamehistory.controller.openapi;

public class OpenApiTags {
    public static final String DIRECT_GAME_REQUEST = "Direct Game Request";
    public static final String INDIRECT_GAME_REQUEST = "Indirect Game Request";
    public static final String GAMEHISTORY = "Game History";
    public static final String STATS = "Stats";
}

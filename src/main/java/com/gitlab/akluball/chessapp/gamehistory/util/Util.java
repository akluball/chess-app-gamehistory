package com.gitlab.akluball.chessapp.gamehistory.util;

import com.gitlab.akluball.chessapp.gamehistory.exception.HttpException;

import javax.ws.rs.core.Response;
import java.util.Objects;

public class Util {
    public static RuntimeException asRuntime(Throwable throwable) {
        return new RuntimeException(throwable);
    }

    public static String bearer(String serializedJwt) {
        return String.format("Bearer %s", serializedJwt);
    }

    public static boolean isOdd(int toCheck) {
        return (toCheck & 1) == 1;
    }

    public static boolean isEven(int toCheck) {
        return !isOdd(toCheck);
    }

    public static boolean is2xx(Response response) {
        return Integer.toString(response.getStatus()).startsWith("2");
    }

    public static long minutesToMillis(int minutes) {
        return minutes * 60_000;
    }

    public static int minutesToSeconds(int minutes) {
        return minutes * 60;
    }

    public static <T> T getExceptionCause(Throwable throwable, Class<T> exceptionType) {
        while (Objects.nonNull(throwable)) {
            if (exceptionType.isInstance(throwable)) {
                return exceptionType.cast(throwable);
            }
            throwable = throwable.getCause();
        }
        return null;
    }

    public static HttpException getHttpExceptionCause(Throwable throwable) {
        return getExceptionCause(throwable, HttpException.class);
    }
}

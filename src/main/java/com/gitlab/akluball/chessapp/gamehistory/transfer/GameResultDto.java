package com.gitlab.akluball.chessapp.gamehistory.transfer;

public class GameResultDto {

    private Integer whiteSideUserId;
    private Integer blackSideUserId;
    private Integer whiteSideUserRatingAdjustment;
    private Integer blackSideUserRatingAdjustment;

    public Integer getWhiteSideUserId() {
        return this.whiteSideUserId;
    }

    public Integer getBlackSideUserId() {
        return this.blackSideUserId;
    }

    public static class Builder {
        private final GameResultDto gameResultDto;

        public Builder() {
            this.gameResultDto = new GameResultDto();
        }

        public Builder whiteSideUserId(Integer userId) {
            this.gameResultDto.whiteSideUserId = userId;
            return this;
        }

        public Builder blackSideUserId(Integer userId) {
            this.gameResultDto.blackSideUserId = userId;
            return this;
        }

        public Builder whiteSideRatingAdjustment(Integer ratingAdjustment) {
            this.gameResultDto.whiteSideUserRatingAdjustment = ratingAdjustment;
            return this;
        }

        public Builder blackSideRatingAdjustment(Integer ratingAdjustment) {
            this.gameResultDto.blackSideUserRatingAdjustment = ratingAdjustment;
            return this;
        }

        public GameResultDto build() {
            return this.gameResultDto;
        }
    }
}

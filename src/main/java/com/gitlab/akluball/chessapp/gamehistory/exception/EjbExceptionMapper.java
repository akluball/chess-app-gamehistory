package com.gitlab.akluball.chessapp.gamehistory.exception;

import javax.ejb.EJBException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.gamehistory.util.Util.getExceptionCause;

@Provider
public class EjbExceptionMapper implements ExceptionMapper<EJBException> {
    @Context
    private Providers providers;

    @Override
    public Response toResponse(EJBException exception) {
        ConstraintViolationException cve = getExceptionCause(exception, ConstraintViolationException.class);
        if (Objects.nonNull(cve)) {
            return this.providers.getExceptionMapper(ConstraintViolationException.class).toResponse(cve);
        } else {
            return Response.status(500).entity("internal server error").build();
        }
    }
}

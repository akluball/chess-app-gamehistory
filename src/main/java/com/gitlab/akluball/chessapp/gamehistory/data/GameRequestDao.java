package com.gitlab.akluball.chessapp.gamehistory.data;

import com.gitlab.akluball.chessapp.gamehistory.GameHistoryConfig;
import com.gitlab.akluball.chessapp.gamehistory.model.DirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.model.IndirectGameRequest;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class GameRequestDao {

    @PersistenceContext(unitName = "gameHistoryPu")
    private EntityManager entityManager;
    @Inject
    private GameHistoryConfig gameHistoryConfig;

    public void createDirect(int requesterId, int requesteeId) {
        long millisPerPlayer = this.gameHistoryConfig.defaultMillisPerPlayer();
        this.entityManager.persist(new DirectGameRequest(requesterId, requesteeId, millisPerPlayer));
    }

    public DirectGameRequest findDirectById(int id) {
        return this.entityManager.find(DirectGameRequest.class, id);
    }

    public List<DirectGameRequest> getDirectRequestsTo(int requesteeId) {
        return this.entityManager.createNamedQuery(DirectGameRequest.Query.Name.GET_INCOMING, DirectGameRequest.class)
                .setParameter(DirectGameRequest.Query.Param.REQUESTEE_ID, requesteeId)
                .getResultList();
    }

    public List<DirectGameRequest> getDirectRequestsFrom(int requesterId) {
        return this.entityManager.createNamedQuery(DirectGameRequest.Query.Name.GET_OUTGOING, DirectGameRequest.class)
                .setParameter(DirectGameRequest.Query.Param.REQUESTER_ID, requesterId)
                .getResultList();
    }

    public void deleteDirect(DirectGameRequest gameRequest) {
        this.entityManager.remove(gameRequest);
    }

    public IndirectGameRequest createDetachedIndirect(int requesterId) {
        return new IndirectGameRequest(requesterId, this.gameHistoryConfig.defaultMillisPerPlayer());
    }

    public void persistDetachedIndirect(IndirectGameRequest indirectGameRequest) {
        this.entityManager.persist(indirectGameRequest);
    }

    public IndirectGameRequest findIndirectById(int id) {
        return this.entityManager.find(IndirectGameRequest.class, id);
    }

    public IndirectGameRequest findFulfillingIndirectRequest(IndirectGameRequest indirectGameRequest) {
        return this.entityManager
                .createNamedQuery(IndirectGameRequest.Query.Name.GET_FULFILLING, IndirectGameRequest.class)
                .setParameter(IndirectGameRequest.Query.Param.REQUESTER_ID, indirectGameRequest.getRequesterId())
                .getResultStream().findFirst().orElse(null);
    }

    public List<IndirectGameRequest> getIndirectRequestsFrom(int requesterId) {
        return this.entityManager
                .createNamedQuery(IndirectGameRequest.Query.Name.GET_OUTGOING, IndirectGameRequest.class)
                .setParameter(IndirectGameRequest.Query.Param.REQUESTER_ID, requesterId)
                .getResultList();
    }

    public void deleteIndirect(IndirectGameRequest indirectGameRequest) {
        this.entityManager.remove(indirectGameRequest);
    }
}

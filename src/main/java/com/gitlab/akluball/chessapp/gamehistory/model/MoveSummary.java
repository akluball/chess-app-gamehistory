package com.gitlab.akluball.chessapp.gamehistory.model;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class MoveSummary {
    private String pieceLetter;
    private String promotionPieceLetter;
    private String source;
    private String target;
    private boolean isCapture;
    @Enumerated(EnumType.STRING)
    private SourceDisambiguation sourceDisambiguation;
    private long durationMillis;
    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    public Dto toDto() {
        Dto dto = new Dto();
        dto.pieceLetter = this.pieceLetter;
        dto.promotionPieceLetter = this.promotionPieceLetter;
        dto.source = this.source;
        dto.target = this.target;
        dto.isCapture = this.isCapture;
        dto.sourceDisambiguation = this.sourceDisambiguation;
        dto.durationMillis = this.durationMillis;
        dto.gameStatus = this.gameStatus;
        return dto;
    }

    public static class Dto {
        private String pieceLetter;
        private String promotionPieceLetter;
        private String source;
        private String target;
        private boolean isCapture;
        private SourceDisambiguation sourceDisambiguation;
        private long durationMillis;
        private GameStatus gameStatus;

        public MoveSummary toMoveSummary() {
            MoveSummary moveSummary = new MoveSummary();
            moveSummary.pieceLetter = this.pieceLetter;
            moveSummary.promotionPieceLetter = this.promotionPieceLetter;
            moveSummary.source = this.source;
            moveSummary.target = this.target;
            moveSummary.isCapture = this.isCapture;
            moveSummary.sourceDisambiguation = this.sourceDisambiguation;
            moveSummary.durationMillis = this.durationMillis;
            moveSummary.gameStatus = this.gameStatus;
            return moveSummary;
        }
    }
}

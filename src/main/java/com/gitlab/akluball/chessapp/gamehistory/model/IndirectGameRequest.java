package com.gitlab.akluball.chessapp.gamehistory.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(
                name = IndirectGameRequest.Query.Name.GET_FULFILLING,
                query = IndirectGameRequest.Query.GET_FULFILLING
        ),
        @NamedQuery(
                name = IndirectGameRequest.Query.Name.GET_OUTGOING,
                query = IndirectGameRequest.Query.GET_OUTGOING
        )
})
public class IndirectGameRequest {
    @Id
    @GeneratedValue
    private int id;
    private int requesterId;
    private long millisPerPlayer;

    public IndirectGameRequest() {
    }

    public IndirectGameRequest(int requesterId, long millisPerPlayer) {
        this.requesterId = requesterId;
        this.millisPerPlayer = millisPerPlayer;
    }

    public int getRequesterId() {
        return this.requesterId;
    }

    public long getMillisPerPlayer() {
        return millisPerPlayer;
    }

    public boolean isRequester(int userId) {
        return this.requesterId == userId;
    }

    public Dto toDto() {
        return this.new Dto();
    }

    public static class Query {
        public static final String GET_FULFILLING
                = "SELECT request FROM IndirectGameRequest AS request"
                + " WHERE request.requesterId!=:" + Param.REQUESTER_ID;
        public static final String GET_OUTGOING
                = "SELECT request FROM IndirectGameRequest AS request"
                + " WHERE request.requesterId=:" + Param.REQUESTER_ID;

        public static class Name {
            public static final String GET_FULFILLING = "getFulfilling";
            public static final String GET_OUTGOING = "getOutgoing";
        }

        public static class Param {
            public static final String REQUESTER_ID = "requesterId";
        }
    }

    @Schema(name = "IndirectGameRequest")
    public class Dto {
        private int id;
        private int requesterId;

        private Dto() {
            IndirectGameRequest indirectGameRequest = IndirectGameRequest.this;
            this.id = indirectGameRequest.id;
            this.requesterId = indirectGameRequest.requesterId;
        }
    }
}

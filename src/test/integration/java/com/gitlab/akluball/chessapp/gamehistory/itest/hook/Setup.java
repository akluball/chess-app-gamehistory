package com.gitlab.akluball.chessapp.gamehistory.itest.hook;

import com.gitlab.akluball.chessapp.gamehistory.itest.TestConfig;

import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;

import static com.gitlab.akluball.chessapp.gamehistory.itest.TestConfig.*;
import static com.gitlab.akluball.chessapp.gamehistory.itest.hook.DeployUtil.*;
import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.sleepMillis;

public class Setup {
    public static void main(String[] args) throws IOException, InterruptedException, NoSuchAlgorithmException {
        // keys
        Base64.Encoder encoder = Base64.getEncoder();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        // gameHistory keys
        KeyPair gameHistoryKeyPair = keyPairGenerator.generateKeyPair();
        String gameHistoryPrivateKey = new String(encoder.encode(gameHistoryKeyPair.getPrivate().getEncoded()));
        String gameHistoryPublicKey = new String(encoder.encode(gameHistoryKeyPair.getPublic().getEncoded()));
        // user keys
        KeyPair userKeyPair = keyPairGenerator.generateKeyPair();
        String userPrivateKey = new String(encoder.encode(userKeyPair.getPrivate().getEncoded()));
        String userPublicKey = new String(encoder.encode(userKeyPair.getPublic().getEncoded()));
        // gamestate keys
        KeyPair gameStateKeyPair = keyPairGenerator.generateKeyPair();
        String gameStatePrivateKey = new String(encoder.encode(gameStateKeyPair.getPrivate().getEncoded()));
        String gameStatePublicKey = new String(encoder.encode(gameStateKeyPair.getPublic().getEncoded()));
        // messaging keys
        KeyPair messagingKeyPair = keyPairGenerator.generateKeyPair();
        String messagingPrivateKey = new String(encoder.encode(messagingKeyPair.getPrivate().getEncoded()));
        String messagingPublicKey = new String(encoder.encode(messagingKeyPair.getPublic().getEncoded()));
        // test properties
        Properties testProps = new Properties();
        testProps.put(GAMEHISTORY_PUBLICKEY_PROP, gameHistoryPublicKey);
        testProps.put(USER_PRIVATEKEY_PROP, userPrivateKey);
        testProps.put(GAMESTATE_PRIVATEKEY_PROP, gameStatePrivateKey);
        testProps.put(MESSAGING_PRIVATEKEY_PROP, messagingPrivateKey);
        FileWriter testPropsWriter = new FileWriter(System.getenv("GAMEHISTORY_TEST_PROPERTIES"));
        testProps.store(testPropsWriter, null);
        // containers
        new ProcessBuilder("docker", "network", "create", NETWORK_NAME)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", DB_CONTAINER, "--network", NETWORK_NAME,
                "--env", String.format("POSTGRES_DB=%s", DB_NAME),
                "--env", String.format("POSTGRES_USER=%s", DB_USER),
                "--env", String.format("POSTGRES_PASSWORD=%s", DB_PASSWORD),
                "--rm", "--detach",
                POSTGRES_IMAGE,
                "-c", "max_connections=200")
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", GAMESTATE_CONTAINER, "--network", NETWORK_NAME,
                "--detach", "--rm", MOCKSERVER_IMAGE)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", USER_CONTAINER, "--network", NETWORK_NAME,
                "--detach", "--rm", MOCKSERVER_IMAGE)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", PAYARA_CONTAINER, "--network", NETWORK_NAME,
                "--env", String.format("DB_SERVER=%s", getDbIp()),
                "--env", String.format("DB_NAME=%s", DB_NAME),
                "--env", String.format("DB_USER=%s", DB_USER),
                "--env", String.format("DB_PASSWORD=%s", DB_PASSWORD),
                "--env", String.format("CHESSAPP_GAMESTATE_URI=%s", getGameStateUri()),
                "--env", String.format("CHESSAPP_USER_URI=%s", getUserUri()),
                "--env", String.format("CHESSAPP_GAMEHISTORY_PRIVATEKEY=%s", gameHistoryPrivateKey),
                "--env", String.format("CHESSAPP_USER_PUBLICKEY=%s", userPublicKey),
                "--env", String.format("CHESSAPP_GAMESTATE_PUBLICKEY=%s", gameStatePublicKey),
                "--env", String.format("CHESSAPP_MESSAGING_PUBLICKEY=%s", messagingPublicKey),
                "--env", String.format("CHESSAPP_GAMEHISTORY_ELOKFACTOR=%s", TestConfig.ELO_RATING_K_FACTOR),
                "--env", String.format("CHESSAPP_GAMEHISTORY_MILLISPERPLAYER=%s", TestConfig.DEFAULT_MILLIS_PER_PLAYER),
                "--rm", "--detach", PAYARA_IMAGE)
                .inheritIO().start().waitFor();
        String gameHistoryWar = System.getenv("CHESSAPP_GAMEHISTORY_WAR");
        copyToContainer(gameHistoryWar, PAYARA_CONTAINER, CONTAINER_GAMEHISTORY_WAR);
        // wait for payara to come up
        int uptimeAttempts = 6;
        while (uptimeAttempts > 0 && !containerAsadmin("uptime")) {
            sleepMillis(500);
            uptimeAttempts--;
        }
        containerAsadmin("deploy", "--name", GAMEHISTORY_DEPLOY_NAME, CONTAINER_GAMEHISTORY_WAR);
    }
}

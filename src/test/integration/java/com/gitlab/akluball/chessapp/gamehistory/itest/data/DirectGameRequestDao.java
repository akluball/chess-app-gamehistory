package com.gitlab.akluball.chessapp.gamehistory.itest.data;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.DirectGameRequest;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.stream.Stream;

public class DirectGameRequestDao {
    private final EntityManager entityManager;

    @Inject
    DirectGameRequestDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public DirectGameRequest findById(int directRequestId) {
        return this.entityManager.find(DirectGameRequest.class, directRequestId);
    }

    public DirectGameRequest getBetween(int requesterId, int requesteeId) {
        String query = String
                .format("SELECT req FROM DirectGameRequest req WHERE req.requesterId=%s AND req.requesteeId=%s",
                        requesterId,
                        requesteeId);
        return this.entityManager.createQuery(query, DirectGameRequest.class)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    public void persist(DirectGameRequest... directGameRequests) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        Stream.of(directGameRequests).forEach(this.entityManager::persist);
        tx.commit();
    }

    public void detach(DirectGameRequest directGameRequest) {
        this.entityManager.detach(directGameRequest);
    }

    public void delete(DirectGameRequest directGameRequest) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.remove(directGameRequest);
        tx.commit();
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest.client;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.DirectGameRequest;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.gamehistory.itest.security.SecurityUtil.bearer;

public class DirectGameRequestClient {
    private final WebTarget target;

    @Inject
    DirectGameRequestClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target("gamerequest");
    }

    public Response create(String token, String potentialOpponentUserId) {
        WebTarget target = this.target.path("direct");
        if (Objects.nonNull(potentialOpponentUserId)) {
            target = target.queryParam("to", potentialOpponentUserId);
        }
        Invocation.Builder invocationBuilder = target.request();
        bearer(invocationBuilder, token);
        return invocationBuilder.post(null);
    }

    public Response create(String token, int potentialOpponentUserId) {
        return this.create(token, "" + potentialOpponentUserId);
    }

    public Response getIncoming(String token) {
        Invocation.Builder invocationBuilder = this.target.path("direct/incoming").request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response getOutgoing(String token) {
        Invocation.Builder invocationBuilder = this.target.path("direct/outgoing").request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response decline(String token, String directRequestId) {
        Invocation.Builder invocationBuilder = this.target.path("direct").path(directRequestId)
                .path("decline")
                .request();
        bearer(invocationBuilder, token);
        return invocationBuilder.post(null);
    }

    public Response decline(String token, DirectGameRequest directGameRequest) {
        return this.decline(token, "" + directGameRequest.getId());
    }

    public Response accept(String token, String directRequestId) {
        Invocation.Builder invocationBuilder = this.target.path("direct").path(directRequestId)
                .path("accept")
                .request();
        bearer(invocationBuilder, token);
        return invocationBuilder.post(null);
    }

    public Response accept(String token, DirectGameRequest directRequest) {
        return this.accept(token, "" + directRequest.getId());
    }

    public Response cancel(String token, String directRequestId) {
        Invocation.Builder invocationBuilder = this.target.path("direct").path(directRequestId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.delete();
    }

    public Response cancel(String token, DirectGameRequest directRequest) {
        return this.cancel(token, "" + directRequest.getId());
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest.mock;

import com.gitlab.akluball.chessapp.gamehistory.itest.TestConfig;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.GameStateCreateDto;
import com.nimbusds.jwt.SignedJWT;
import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.MatchType;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.JsonBody;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.bind.Jsonb;

import static com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUtil.extractToken;
import static com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUtil.verifyWithRetry;

@Singleton
public class MockGameState {
    private final Jsonb jsonb;
    private final MockServerClient client;

    @Inject
    MockGameState(Jsonb jsonb, TestConfig testConfig) {
        this.jsonb = jsonb;
        this.client = new MockServerClient(testConfig.gameStateHost(), testConfig.gameStatePort());
    }

    public void reset() {
        this.client.reset();
    }

    private HttpRequest createPut(GameStateCreateDto gameStateCreateDto) {
        JsonBody body = JsonBody.json(this.jsonb.toJson(gameStateCreateDto), MatchType.ONLY_MATCHING_FIELDS);
        return HttpRequest.request().withMethod("PUT").withPath("/api/gamestate/.*").withBody(body);
    }

    public void expectPut(GameStateCreateDto gameStateCreateDto) {
        HttpRequest req = this.createPut(gameStateCreateDto);
        HttpResponse res = HttpResponse.response().withStatusCode(201);
        this.client.when(req).respond(res);
    }

    public void expectPutFail(GameStateCreateDto gameStateCreateDto) {
        HttpRequest req = this.createPut(gameStateCreateDto);
        HttpResponse res = HttpResponse.response().withStatusCode(500);
        this.client.when(req).respond(res);
    }

    public SignedJWT retrievePutToken(GameStateCreateDto gameStateCreateDto) {
        return extractToken(this.client, this.createPut(gameStateCreateDto));
    }

    public void verifyPut(GameStateCreateDto gameStateCreateDto) {
        verifyWithRetry(this.client, this.createPut(gameStateCreateDto));
        this.clearPut(gameStateCreateDto);
    }

    public void clearPut(GameStateCreateDto gameStateCreateDto) {
        this.client.clear(this.createPut(gameStateCreateDto));
    }
}

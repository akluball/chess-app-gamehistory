package com.gitlab.akluball.chessapp.gamehistory.itest.data;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.stream.Stream;

public class GameHistoryDao {
    private final EntityManager entityManager;

    @Inject
    GameHistoryDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(GameHistory... gameHistories) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        Stream.of(gameHistories).forEach(this.entityManager::persist);
        tx.commit();
    }

    public void refresh(GameHistory gameHistory) {
        this.entityManager.refresh(gameHistory);
    }

    public List<GameHistory> findInvolving(int userOneId, int userTwoId) {
        return this.entityManager.createNamedQuery(GameHistory.Query.Name.FIND_BY_PARTICIPANTS, GameHistory.class)
                .setParameter(GameHistory.Query.Param.USER_ONE_ID, userOneId)
                .setParameter(GameHistory.Query.Param.USER_TWO_ID, userTwoId)
                .getResultList();
    }

    public void delete(GameHistory gameHistory) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.remove(gameHistory);
        tx.commit();
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Access(AccessType.FIELD)
public class DirectGameRequest {
    @Id
    @GeneratedValue
    private Integer id;
    private Integer requesterId;
    private Integer requesteeId;
    private Long epochSeconds;
    private Long millisPerPlayer;

    public Integer getId() {
        return id;
    }

    public Long getEpochSeconds() {
        return epochSeconds;
    }

    public Long getMillisPerPlayer() {
        return millisPerPlayer;
    }

    public static class Builder {
        private final DirectGameRequest directGameRequest;

        Builder() {
            this.directGameRequest = new DirectGameRequest();
        }

        public Builder from(Integer requesterId) {
            this.directGameRequest.requesterId = requesterId;
            return this;
        }

        public Builder to(Integer requesteeId) {
            this.directGameRequest.requesteeId = requesteeId;
            return this;
        }

        public Builder epochSeconds(Long epochSeconds) {
            this.directGameRequest.epochSeconds = epochSeconds;
            return this;
        }

        public Builder millisPerPlayer(Long millisPerPlayer) {
            this.directGameRequest.millisPerPlayer = millisPerPlayer;
            return this;
        }

        public DirectGameRequest build() {
            return this.directGameRequest;
        }
    }
}

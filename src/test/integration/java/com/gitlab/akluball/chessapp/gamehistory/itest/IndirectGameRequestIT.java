package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.IndirectGameRequestClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.GameHistoryDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.IndirectGameRequestDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockGameState;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUser;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.IndirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.GameStateCreateDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.IndirectGameRequestDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.ResourceLock;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class IndirectGameRequestIT {
    public static final String INDIRECT_GAME_REQUEST_RESOURCE_LOCK = "indirect-game-request";

    private final UniqueData uniqueData;
    private final Builders builders;
    private final IndirectGameRequestDao indirectGameRequestDao;
    private final GameHistoryDao gameHistoryDao;
    private final UserSecurity userSecurity;
    private final MockUser mockUser;
    private final MockGameState mockGameState;
    private final IndirectGameRequestClient indirectGameRequestClient;

    @Guicy
    IndirectGameRequestIT(UniqueData uniqueData,
            Builders builders,
            IndirectGameRequestDao indirectGameRequestDao,
            GameHistoryDao gameHistoryDao,
            UserSecurity userSecurity,
            MockUser mockUser,
            MockGameState mockGameState,
            IndirectGameRequestClient indirectGameRequestClient) {
        this.uniqueData = uniqueData;
        this.builders = builders;
        this.indirectGameRequestDao = indirectGameRequestDao;
        this.gameHistoryDao = gameHistoryDao;
        this.userSecurity = userSecurity;
        this.mockUser = mockUser;
        this.mockGameState = mockGameState;
        this.indirectGameRequestClient = indirectGameRequestClient;
    }

    @BeforeEach
    void cleanIndirect(TestInfo testInfo) {
        Method method = testInfo.getTestMethod().orElse(null);
        if (method != null) {
            ResourceLock annotation = method.getAnnotation(ResourceLock.class);
            if ((annotation != null) && (INDIRECT_GAME_REQUEST_RESOURCE_LOCK.equals(annotation.value()))) {
                this.indirectGameRequestDao.deleteAll();
            }
        }
    }

    @Test
    void createIndirectNoAuth() {
        Response response = this.indirectGameRequestClient.create(null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createIndirectBadRole() {
        String badRoleToken = this.userSecurity.badRoleTokenFor(this.uniqueData.userId());
        Response response = this.indirectGameRequestClient.create(badRoleToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createIndirectSignatureRole() {
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(this.uniqueData.userId());
        Response response = this.indirectGameRequestClient.create(badSignatureToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    @Guicy
    void createPersistsOnNoMatch(TestConfig testConfig) {
        int userId = this.uniqueData.userId();
        Response response = this.indirectGameRequestClient.create(this.userSecurity.tokenFor(userId));
        assertThat(response.getStatus()).isEqualTo(204);
        List<IndirectGameRequest> requests = this.indirectGameRequestDao.findByUserId(userId);
        assertThat(requests).hasSize(1);
        assertThat(requests.get(0).getMillisPerPlayer()).isEqualTo(testConfig.defaultMillisPerPlayer());
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void createFirstRequesterNotFound() {
        int userOneId = this.uniqueData.userId();
        int userTwoId = this.uniqueData.userId();
        this.indirectGameRequestDao.persist(this.builders.indirectGameRequest().requester(userOneId).build());
        this.mockUser.expectGetNotFound(userOneId);
        this.mockUser.expectGet(userTwoId);
        Response response = this.indirectGameRequestClient.create(this.userSecurity.tokenFor(userTwoId));
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
        this.mockUser.clearGet(userOneId);
        this.mockUser.clearGet(userTwoId);
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void createSecondRequesterNotFound() {
        int userOneId = this.uniqueData.userId();
        int userTwoId = this.uniqueData.userId();
        this.indirectGameRequestDao.persist(this.builders.indirectGameRequest().requester(userOneId).build());
        this.mockUser.expectGet(userOneId);
        this.mockUser.expectGetNotFound(userTwoId);
        Response response = this.indirectGameRequestClient.create(this.userSecurity.tokenFor(userTwoId));
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
        this.mockUser.clearGet(userOneId);
        this.mockUser.clearGet(userTwoId);
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void createPersistsGameHistoryOnMatch() {
        int requesterOneId = this.uniqueData.userId();
        int requesterTwoId = this.uniqueData.userId();
        IndirectGameRequest indirectGameRequest = this.builders.indirectGameRequest().requester(requesterOneId)
                .millisPerPlayer(600_000L).build();
        this.indirectGameRequestDao.persist(indirectGameRequest);
        this.mockUser.expectGet(requesterOneId);
        this.mockUser.expectGet(requesterTwoId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesterOneId)
                .blackSideUserId(requesterTwoId).build();
        this.mockGameState.expectPut(createDto);
        Response response = this.indirectGameRequestClient.create(this.userSecurity.tokenFor(requesterTwoId));
        assertThat(response.getStatus()).isEqualTo(204);
        List<GameHistory> gameHistories = this.gameHistoryDao.findInvolving(requesterOneId, requesterTwoId);
        assertThat(gameHistories).hasSize(1);
        GameHistory created = gameHistories.get(0);
        assertThat(created.getWhiteSideUserId()).isEqualTo(requesterOneId);
        assertThat(created.getBlackSideUserId()).isEqualTo(requesterTwoId);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterOneId);
        this.mockUser.clearGet(requesterTwoId);
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void createCreatesGameStateOnMatch() {
        int requesterOneId = this.uniqueData.userId();
        int requesterTwoId = this.uniqueData.userId();
        IndirectGameRequest indirectGameRequest = this.builders.indirectGameRequest().requester(requesterOneId)
                .millisPerPlayer(600_000L).build();
        this.indirectGameRequestDao.persist(indirectGameRequest);
        this.mockUser.expectGet(requesterOneId);
        this.mockUser.expectGet(requesterTwoId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesterOneId)
                .blackSideUserId(requesterTwoId).build();
        this.mockGameState.expectPut(createDto);
        Response response = this.indirectGameRequestClient.create(this.userSecurity.tokenFor(requesterTwoId));
        assertThat(response.getStatus()).isEqualTo(204);
        this.mockGameState.verifyPut(createDto);
        this.mockUser.clearGet(requesterOneId);
        this.mockUser.clearGet(requesterTwoId);
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void createDeletesRequestsOnMatch() {
        int requesterOneId = this.uniqueData.userId();
        int requesterTwoId = this.uniqueData.userId();
        IndirectGameRequest indirectGameRequest = this.builders.indirectGameRequest().requester(requesterOneId)
                .millisPerPlayer(600_000L).build();
        this.indirectGameRequestDao.persist(indirectGameRequest);
        this.mockUser.expectGet(requesterOneId);
        this.mockUser.expectGet(requesterTwoId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesterOneId)
                .blackSideUserId(requesterTwoId).build();
        this.mockGameState.expectPut(createDto);
        Response response = this.indirectGameRequestClient.create(this.userSecurity.tokenFor(requesterTwoId));
        assertThat(response.getStatus()).isEqualTo(204);
        assertThat(this.indirectGameRequestDao.findByUserId(requesterOneId).isEmpty()).isTrue();
        assertThat(this.indirectGameRequestDao.findByUserId(requesterTwoId).isEmpty()).isTrue();
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterOneId);
        this.mockUser.clearGet(requesterTwoId);
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void createMultipleForSameUser() {
        int userId = this.uniqueData.userId();
        String token = this.userSecurity.tokenFor(userId);
        Response responseOne = this.indirectGameRequestClient.create(token);
        assertThat(responseOne.getStatus()).isEqualTo(204);
        Response responseTwo = this.indirectGameRequestClient.create(token);
        assertThat(responseTwo.getStatus()).isEqualTo(204);
        assertThat(this.indirectGameRequestDao.findByUserId(userId)).hasSize(2);
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void getOutgoing() {
        int userId = this.uniqueData.userId();
        IndirectGameRequest requestOne = this.builders.indirectGameRequest().requester(userId).build();
        IndirectGameRequest requestTwo = this.builders.indirectGameRequest().requester(userId).build();
        this.indirectGameRequestDao.persist(requestOne, requestTwo);
        Response response = this.indirectGameRequestClient.getOutgoing(this.userSecurity.tokenFor(userId));
        assertThat(response.getStatus()).isEqualTo(200);
        Set<IndirectGameRequestDto> outgoing = response.readEntity(new GenericType<Set<IndirectGameRequestDto>>() {});
        assertThat(outgoing).hasSize(2);
    }

    @Test
    void getOutgoingNoAuth() {
        Response response = this.indirectGameRequestClient.getOutgoing(null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void cancel() {
        int userId = this.uniqueData.userId();
        IndirectGameRequest request = this.builders.indirectGameRequest().requester(userId).build();
        this.indirectGameRequestDao.persist(request);
        Response response = this.indirectGameRequestClient.cancel(this.userSecurity.tokenFor(userId), request);
        assertThat(response.getStatus()).isEqualTo(204);
        assertThat(this.indirectGameRequestDao.findByUserId(userId)).hasSize(0);
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void cancelNoAuth() {
        IndirectGameRequest request = this.builders.indirectGameRequest().requester(this.uniqueData.userId()).build();
        this.indirectGameRequestDao.persist(request);
        Response response = this.indirectGameRequestClient.cancel(null, request);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void cancelNonIntegerRequestId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.indirectGameRequestClient.cancel(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void cancelNotRequester() {
        IndirectGameRequest request = this.builders.indirectGameRequest().requester(this.uniqueData.userId()).build();
        this.indirectGameRequestDao.persist(request);
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.indirectGameRequestClient.cancel(token, request);
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    @ResourceLock(INDIRECT_GAME_REQUEST_RESOURCE_LOCK)
    void cancelRequestNotFound() {
        int userId = this.uniqueData.userId();
        IndirectGameRequest request = this.builders.indirectGameRequest().requester(userId).build();
        this.indirectGameRequestDao.persist(request);
        this.indirectGameRequestDao.delete(request);
        Response response = this.indirectGameRequestClient.cancel(this.userSecurity.tokenFor(userId), request);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest.security;

import com.gitlab.akluball.chessapp.gamehistory.itest.TestConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;

import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.asRuntime;

public class GameStateSecurity {
    private final String token;
    private final String expiredToken;

    @Inject
    GameStateSecurity(TestConfig testConfig) {
        RSASSASigner signer = new RSASSASigner(testConfig.gameStatePrivateKey());
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(ServiceName.GAMESTATE.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .expirationTime(Date.from(Instant.now().plusSeconds(60)))
                .build();
        JWTClaimsSet expiredClaimsSet = new JWTClaimsSet.Builder()
                .subject(ServiceName.GAMESTATE.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .expirationTime(Date.from(Instant.now().minusSeconds(60)))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        SignedJWT expiredToken = new SignedJWT(header, expiredClaimsSet);
        try {
            token.sign(signer);
            expiredToken.sign(signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        this.token = token.serialize();
        this.expiredToken = expiredToken.serialize();
    }

    public String getToken() {
        return this.token;
    }

    public String getExpiredToken() {
        return this.expiredToken;
    }
}

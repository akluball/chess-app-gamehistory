package com.gitlab.akluball.chessapp.gamehistory.itest.transfer;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.SourceDisambiguation;

public class MoveSummaryDto {
    private String pieceLetter;
    private String promotionPieceLetter;
    private String source;
    private String target;
    private boolean isCapture;
    private SourceDisambiguation sourceDisambiguation;
    private long durationMillis;
    private GameStatus gameStatus;

    public String getPieceLetter() {
        return pieceLetter;
    }

    public String getPromotionPieceLetter() {
        return promotionPieceLetter;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    public boolean isCapture() {
        return isCapture;
    }

    public SourceDisambiguation getSourceDisambiguation() {
        return sourceDisambiguation;
    }

    public long getDurationMillis() {
        return durationMillis;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }
}

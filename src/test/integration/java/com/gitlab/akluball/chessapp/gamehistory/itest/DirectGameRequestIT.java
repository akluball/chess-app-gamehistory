package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.DirectGameRequestClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.DirectGameRequestDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockGameState;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUser;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.DirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.GameStateCreateDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;

import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.bufferBelow4;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class DirectGameRequestIT {
    private final UniqueData uniqueData;
    private final Builders builders;
    private final DirectGameRequestDao directGameRequestDao;
    private final MockUser mockUser;
    private final MockGameState mockGameState;
    private final UserSecurity userSecurity;
    private final DirectGameRequestClient directGameRequestClient;

    @Guicy
    DirectGameRequestIT(UniqueData uniqueData,
            Builders builders,
            DirectGameRequestDao directGameRequestDao,
            MockUser mockUser,
            MockGameState mockGameState,
            UserSecurity userSecurity,
            DirectGameRequestClient directGameRequestClient) {
        this.uniqueData = uniqueData;
        this.builders = builders;
        this.directGameRequestDao = directGameRequestDao;
        this.mockUser = mockUser;
        this.mockGameState = mockGameState;
        this.userSecurity = userSecurity;
        this.directGameRequestClient = directGameRequestClient;
    }

    @Test
    void createPersistsDirectRequest() {
        int userId = this.uniqueData.userId();
        int requesteeUserId = this.uniqueData.userId();
        this.mockUser.expectGet(requesteeUserId);
        this.directGameRequestClient.create(this.userSecurity.tokenFor(userId), requesteeUserId);
        DirectGameRequest persisted = this.directGameRequestDao.getBetween(userId, requesteeUserId);
        assertThat(persisted).isNotNull();
        this.mockUser.clearGet(requesteeUserId);
    }

    @Test
    void createSetsEpochSeconds() {
        int userId = this.uniqueData.userId();
        int requesteeUserId = this.uniqueData.userId();
        this.mockUser.expectGet(requesteeUserId);
        this.directGameRequestClient.create(this.userSecurity.tokenFor(userId), requesteeUserId);
        DirectGameRequest persisted = this.directGameRequestDao.getBetween(userId, requesteeUserId);
        assertThat(persisted.getEpochSeconds()).isIn(bufferBelow4(Instant.now().getEpochSecond()));
        this.mockUser.clearGet(requesteeUserId);
    }

    @Test
    @Guicy
    void createSetsMillisPerPlayer(TestConfig testConfig) {
        int userId = this.uniqueData.userId();
        int requesteeUserId = this.uniqueData.userId();
        this.mockUser.expectGet(requesteeUserId);
        this.directGameRequestClient.create(this.userSecurity.tokenFor(userId), requesteeUserId);
        DirectGameRequest persisted = this.directGameRequestDao.getBetween(userId, requesteeUserId);
        assertThat(persisted.getMillisPerPlayer()).isEqualTo(testConfig.defaultMillisPerPlayer());
        this.mockUser.clearGet(requesteeUserId);
    }

    @Test
    void declineDeletes() {
        int userId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().to(userId).build();
        this.directGameRequestDao.persist(directRequest);
        this.directGameRequestClient.decline(this.userSecurity.tokenFor(userId), directRequest);
        this.directGameRequestDao.detach(directRequest);
        assertThat(this.directGameRequestDao.findById(directRequest.getId())).isNull();
    }

    @Test
    void acceptDeletes() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        String token = this.userSecurity.tokenFor(requesteeId);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        this.directGameRequestClient.accept(token, directRequest);
        this.directGameRequestDao.detach(directRequest);
        assertThat(this.directGameRequestDao.findById(directRequest.getId())).isNull();
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void cancelDeletes() {
        int userId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(userId).build();
        this.directGameRequestDao.persist(directRequest);
        this.directGameRequestClient.cancel(this.userSecurity.tokenFor(userId), directRequest);
        this.directGameRequestDao.detach(directRequest);
        assertThat(this.directGameRequestDao.findById(directRequest.getId())).isNull();
    }
}

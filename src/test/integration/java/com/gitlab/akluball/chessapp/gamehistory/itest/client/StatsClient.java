package com.gitlab.akluball.chessapp.gamehistory.itest.client;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static com.gitlab.akluball.chessapp.gamehistory.itest.security.SecurityUtil.bearer;

public class StatsClient {
    private final WebTarget target;

    @Inject
    StatsClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target("stats");
    }

    public Response getStats(String token, String userId) {
        Invocation.Builder invocationBuilder = this.target.path("user").path(userId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response getStats(String token, int userId) {
        return this.getStats(token, "" + userId);
    }

    public Response getVersusStats(String token, String opponentUserId) {
        Invocation.Builder invocationBuilder = this.target.path("versus").path(opponentUserId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response getVersusStats(String token, int opponentUserId) {
        return this.getVersusStats(token, "" + opponentUserId);
    }
}

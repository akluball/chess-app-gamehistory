package com.gitlab.akluball.chessapp.gamehistory.itest.client;

import com.gitlab.akluball.chessapp.gamehistory.itest.TestConfig;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

public class ApiWrapper {
    private final WebTarget target;

    @Inject
    ApiWrapper(Client client, TestConfig testConfig) {
        this.target = client.target(String.format("%s/api", testConfig.gameHistoryUri()));
    }

    public WebTarget target(String path) {
        return this.target.path(path);
    }
}

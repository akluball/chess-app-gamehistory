package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.GameHistoryClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.GameHistoryDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUser;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.ResultTag;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.TerminationTag;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.BearerRole;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.GameHistorySecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.GameStateSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.MessagingSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.ServiceName;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.EndGameDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.GameHistoryDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.akluball.chessapp.gamehistory.itest.security.SecurityUtil.extractBearerRoles;
import static com.gitlab.akluball.chessapp.gamehistory.itest.security.SecurityUtil.extractSubject;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameHistoryControllerIT {
    private final Builders builders;
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameHistoryDao gameHistoryDao;
    private final UserSecurity userSecurity;
    private final GameStateSecurity gameStateSecurity;
    private final GameHistoryClient gameHistoryClient;

    @Guicy
    GameHistoryControllerIT(Builders builders,
            UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameHistoryDao gameHistoryDao,
            UserSecurity userSecurity,
            GameStateSecurity gameStateSecurity,
            GameHistoryClient gameHistoryClient) {
        this.builders = builders;
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameHistoryDao = gameHistoryDao;
        this.userSecurity = userSecurity;
        this.gameStateSecurity = gameStateSecurity;
        this.gameHistoryClient = gameHistoryClient;
    }

    private GameHistory createPersistedGameHistory() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().build();
        this.gameHistoryDao.persist(gameHistory);
        return gameHistory;
    }

    @Test
    void getById() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        String token = this.userSecurity.tokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(GameHistoryDto.class).getId()).isEqualTo(gameHistory.getId());
    }

    @Test
    @Guicy
    void getByIdFromMessaging(MessagingSecurity messagingSecurity) {
        GameHistory gameHistory = this.createPersistedGameHistory();
        Response response = this.gameHistoryClient.getById(messagingSecurity.getToken(), gameHistory);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(GameHistoryDto.class).getId()).isEqualTo(gameHistory.getId());
    }

    @Test
    void getByIdNoAuth() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        Response response = this.gameHistoryClient.getById(null, gameHistory);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getByIdBadRole() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        String badRoleToken = this.userSecurity.badRoleTokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(badRoleToken, gameHistory);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getByIdBadSignature() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(badSignatureToken, gameHistory);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getByIdExpiredToken() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        String expiredToken = this.userSecurity.expiredTokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(expiredToken, gameHistory);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getByIdNonIntegerId() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        String token = this.userSecurity.tokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void getByIdNotParticipant() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void getByIdNotFound() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().build();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void getByParticipantOrdersByStart() {
        int userId = this.uniqueData.userId();
        GameHistory freshest = this.uniqueBuilders.gameHistory().whiteSideUser(userId).secondsSinceStart(5).build();
        GameHistory fresh = this.uniqueBuilders.gameHistory().whiteSideUser(userId).secondsSinceStart(7).build();
        GameHistory stale = this.uniqueBuilders.gameHistory().whiteSideUser(userId).secondsSinceStart(10).build();
        this.gameHistoryDao.persist(fresh, freshest, stale);
        Response response = this.gameHistoryClient
                .getByParticipant(this.userSecurity.tokenFor(userId), (String) null, null);
        assertThat(response.getStatus()).isEqualTo(200);
        List<GameHistoryDto> gameHistories = response.readEntity(new GenericType<List<GameHistoryDto>>() {});
        assertThat(gameHistories.stream().map(GameHistoryDto::getId).toArray(Integer[]::new)).asList()
                .containsExactly(freshest.getId(), fresh.getId(), stale.getId()).inOrder();
    }

    @Test
    void getByParticipantNoAuth() {
        Response response = this.gameHistoryClient.getByParticipant(null, (String) null, null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getByParticipantBadRole() {
        String badRoleToken = this.userSecurity.badRoleTokenFor(this.uniqueData.userId());
        Response response = this.gameHistoryClient.getByParticipant(badRoleToken, (String) null, null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getByParticipantBadSignature() {
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(this.uniqueData.userId());
        Response response = this.gameHistoryClient.getByParticipant(badSignatureToken, (String) null, null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getByParticipantCurrentUserIsCompleted() {
        int userId = this.uniqueData.userId();
        GameHistory completed = this.uniqueBuilders.gameHistory().whiteSideUser(userId).whiteWins().build();
        GameHistory notCompleted = this.uniqueBuilders.gameHistory().whiteSideUser(userId).inProgress().build();
        this.gameHistoryDao.persist(completed, notCompleted);
        String token = this.userSecurity.tokenFor(userId);
        Response response = this.gameHistoryClient.getByParticipant(token, null, true);
        assertThat(response.getStatus()).isEqualTo(200);
        List<GameHistoryDto> completedGames = response.readEntity(new GenericType<List<GameHistoryDto>>() {});
        assertThat(completedGames.stream().map(GameHistoryDto::getId).collect(Collectors.toList()))
                .containsExactly(completed.getId());
    }

    @Test
    void getByParticipantNonCurrentUserIsCompleted() {
        int userId = this.uniqueData.userId();
        GameHistory completed = this.uniqueBuilders.gameHistory().whiteSideUser(userId).whiteWins().build();
        GameHistory notCompleted = this.uniqueBuilders.gameHistory().whiteSideUser(userId).inProgress().build();
        this.gameHistoryDao.persist(completed, notCompleted);
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameHistoryClient.getByParticipant(token, userId, true);
        assertThat(response.getStatus()).isEqualTo(200);
        List<GameHistoryDto> completedGames = response.readEntity(new GenericType<List<GameHistoryDto>>() {});
        assertThat(completedGames.stream().map(GameHistoryDto::getId).collect(Collectors.toList()))
                .containsExactly(completed.getId());
    }

    @Test
    void getByParticipantNonIntegerUserId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameHistoryClient.getByParticipant(token, "not-an-integer", null);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void getByParticipantNonBooleanIsCompleted() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameHistoryClient.getByParticipant(token, null, "not-a-boolean");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    @Guicy
    void endGame(MockUser mockUser) {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto().resultTag(ResultTag.BLACK_WINS)
                .terminationTag(TerminationTag.TIME_FORFEIT).build();
        mockUser.expectEndGamePost(gameHistory);
        Response response = this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(204);
        mockUser.clearEndGamePost(gameHistory);
    }

    @Test
    @Guicy
    void endGameInformsUserServiceAuth(MockUser mockUser, GameHistorySecurity gameHistorySecurity) {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(ResultTag.BLACK_WINS).terminationTag(TerminationTag.NORMAL).build();
        mockUser.expectEndGamePost(gameHistory);
        Response response = this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(204);
        SignedJWT token = mockUser.retrieveEndGamePostToken(gameHistory);
        assertThat(extractBearerRoles(token)).containsExactly(BearerRole.SERVICE);
        assertThat(extractSubject(token)).isEqualTo(ServiceName.GAMEHISTORY.asString());
        assertThat(gameHistorySecurity.isValidSignature(token)).isTrue();
        mockUser.clearEndGamePost(gameHistory);
    }

    @Test
    void endGameNoAuth() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(ResultTag.BLACK_WINS).terminationTag(TerminationTag.NORMAL).build();
        Response response = this.gameHistoryClient.endGame(null, gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void endGameExpiredToken() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(ResultTag.BLACK_WINS).terminationTag(TerminationTag.NORMAL).build();
        Response response = this.gameHistoryClient.endGame(null, gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void endGameNonIntegerId() {
        EndGameDto endGameDto = this.builders.endGameDto().resultTag(ResultTag.BLACK_WINS)
                .terminationTag(TerminationTag.TIME_FORFEIT).build();
        String token = this.gameStateSecurity.getToken();
        Response response = this.gameHistoryClient.endGame(token, "not-an-integer", endGameDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNoEndGameDto() {
        String token = this.gameStateSecurity.getToken();
        Response response = this.gameHistoryClient.endGame(token, this.createPersistedGameHistory(), null);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNoResultTag() {
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(null).terminationTag(TerminationTag.NORMAL).build();
        String token = this.gameStateSecurity.getToken();
        Response response = this.gameHistoryClient.endGame(token, this.createPersistedGameHistory(), endGameDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNoTerminationTag() {
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(ResultTag.BLACK_WINS).terminationTag(null).build();
        String token = this.gameStateSecurity.getToken();
        Response response = this.gameHistoryClient.endGame(token, this.createPersistedGameHistory(), endGameDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNoMoveSummaries() {
        EndGameDto endGameDto = this.builders.endGameDto().moveSummaries(null)
                .resultTag(ResultTag.BLACK_WINS).terminationTag(TerminationTag.NORMAL).build();
        String token = this.gameStateSecurity.getToken();
        Response response = this.gameHistoryClient.endGame(token, this.createPersistedGameHistory(), endGameDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNotFound() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        this.gameHistoryDao.delete(gameHistory);
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(ResultTag.BLACK_WINS).terminationTag(TerminationTag.NORMAL).build();
        Response response = this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void endGameNotInProgress() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().whiteWins().build();
        this.gameHistoryDao.persist(gameHistory);
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(ResultTag.WHITE_WINS).terminationTag(TerminationTag.NORMAL).build();
        Response response = this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameBadResultTag() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(ResultTag.IN_PROGRESS).terminationTag(TerminationTag.NORMAL).build();
        Response response = this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameBadTerminationTag() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto()
                .resultTag(ResultTag.DRAW).terminationTag(TerminationTag.UNTERMINATED).build();
        Response response = this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    @Guicy
    void endGameFailToInformUserService(MockUser mockUser) {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto().resultTag(ResultTag.BLACK_WINS)
                .terminationTag(TerminationTag.NORMAL).build();
        mockUser.expectEndGamePostFail(gameHistory);
        Response response = this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        assertThat(response.getStatus()).isEqualTo(503);
        assertThat(response.readEntity(String.class)).containsMatch("service unavailable");
        mockUser.clearEndGamePost(gameHistory);
    }
}

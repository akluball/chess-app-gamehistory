package com.gitlab.akluball.chessapp.gamehistory.itest.mock;

import com.gitlab.akluball.chessapp.gamehistory.itest.TestConfig;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.UserDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;
import com.nimbusds.jwt.SignedJWT;
import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.MatchType;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.JsonBody;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.bind.Jsonb;

import static com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUtil.extractToken;
import static org.mockserver.model.JsonBody.json;

@Singleton
public class MockUser {
    private final MockServerClient client;
    private final Jsonb jsonb;
    private final Builders builders;

    @Inject
    public MockUser(Jsonb jsonb, Builders builders, TestConfig testConfig) {
        this.jsonb = jsonb;
        this.builders = builders;
        this.client = new MockServerClient(testConfig.userHost(), testConfig.userPort());
    }

    private static HttpRequest createGet(int userId) {
        return HttpRequest.request().withMethod("GET").withPath(String.format("/api/user/%s", userId));
    }

    public void reset() {
        this.client.reset();
    }

    public void expectGet(int userId, UserDto userDto) {
        String body = this.jsonb.toJson(userDto);
        HttpResponse res = HttpResponse.response().withStatusCode(200).withHeader(MockUtil.APPLICATION_JSON)
                .withBody(body);
        this.client.when(createGet(userId)).respond(res);
    }

    public void expectGet(int userId) {
        this.expectGet(userId, this.builders.userDto().id(userId).rating(0).build());
    }

    public void expectGet(UserDto userDto) {
        this.expectGet(userDto.getId(), userDto);
    }

    public void expectGetNotFound(int userId) {
        HttpResponse res = HttpResponse.response().withStatusCode(404);
        this.client.when(createGet(userId)).respond(res);
    }

    public SignedJWT retrieveGetToken(int userId) {
        return extractToken(this.client, createGet(userId));
    }

    public void clearGet(int userId) {
        this.client.clear(createGet(userId));
    }

    public void clearGet(UserDto userDto) {
        this.client.clear(createGet(userDto.getId()));
    }

    private HttpRequest createEndGamePost(GameHistory gameHistory) {
        JsonBody body = json(this.jsonb.toJson(gameHistory.toEndGameUserUpdateDto()), MatchType.ONLY_MATCHING_FIELDS);
        return HttpRequest.request().withMethod("POST").withPath("/api/user/endgame").withBody(body);
    }

    public void expectEndGamePost(GameHistory gameHistory) {
        HttpRequest req = this.createEndGamePost(gameHistory);
        HttpResponse res = HttpResponse.response().withStatusCode(204);
        this.client.when(req).respond(res);
    }

    public void expectEndGamePostFail(GameHistory gameHistory) {
        HttpRequest req = this.createEndGamePost(gameHistory);
        HttpResponse res = HttpResponse.response().withStatusCode(400);
        this.client.when(req).respond(res);
    }

    public SignedJWT retrieveEndGamePostToken(GameHistory gameHistory) {
        return extractToken(this.client, createEndGamePost(gameHistory));
    }

    public void clearEndGamePost(GameHistory gameHistory) {
        this.client.clear(this.createEndGamePost(gameHistory));
    }
}

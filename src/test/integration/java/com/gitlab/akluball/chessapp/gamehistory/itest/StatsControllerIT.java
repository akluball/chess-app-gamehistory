package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.StatsClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class StatsControllerIT {
    private final UniqueData uniqueData;
    private final UserSecurity userSecurity;
    private final StatsClient statsClient;

    @Guicy
    StatsControllerIT(UniqueData uniqueData, UserSecurity userSecurity, StatsClient statsClient) {
        this.uniqueData = uniqueData;
        this.userSecurity = userSecurity;
        this.statsClient = statsClient;
    }

    @Test
    void getUserStats() {
        int userId = this.uniqueData.userId();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.statsClient.getStats(token, userId);
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void getUserStatsNoAuth() {
        Response response = this.statsClient.getStats(null, this.uniqueData.userId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getUserStatsNonIntegerId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.statsClient.getStats(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void getVersusStats() {
        int userId = this.uniqueData.userId();
        int opponentUserId = this.uniqueData.userId();
        Response response = this.statsClient.getVersusStats(this.userSecurity.tokenFor(userId), opponentUserId);
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void getVersusStatsNoAuth() {
        Response response = this.statsClient.getVersusStats(null, this.uniqueData.userId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getVersusStatsNonIntegerId() {
        int userId = this.uniqueData.userId();
        Response response = this.statsClient.getVersusStats(this.userSecurity.tokenFor(userId), "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest.security;

import com.gitlab.akluball.chessapp.gamehistory.itest.TestConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;

public class GameHistorySecurity {
    private RSASSAVerifier verifier;

    @Inject
    GameHistorySecurity(TestConfig testConfig) {
        this.verifier = new RSASSAVerifier(testConfig.gameHistoryPublicKey());
    }

    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }
}

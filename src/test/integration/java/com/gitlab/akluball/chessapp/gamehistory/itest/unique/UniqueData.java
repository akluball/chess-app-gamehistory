package com.gitlab.akluball.chessapp.gamehistory.itest.unique;

import com.gitlab.akluball.chessapp.gamehistory.itest.data.UniqueDataDao;

import javax.inject.Inject;
import java.util.concurrent.atomic.AtomicInteger;

public class UniqueData {
    private final int id;
    private final AtomicInteger userIdCounter;

    @Inject
    UniqueData(UniqueDataDao uniqueDataDao) {
        this.id = uniqueDataDao.nextId();
        int blockStart = this.id * 1_000;
        this.userIdCounter = new AtomicInteger(blockStart);
    }

    public int userId() {
        int userId = this.userIdCounter.getAndIncrement();
        return userId;
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest.security;

import com.gitlab.akluball.chessapp.gamehistory.itest.TestConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;

import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.asRuntime;

public class MessagingSecurity {
    private final String token;

    @Inject
    MessagingSecurity(TestConfig testConfig) {
        RSASSASigner signer = new RSASSASigner(testConfig.messagingPrivateKey());
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(ServiceName.MESSAGING.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .expirationTime(Date.from(Instant.now().plusSeconds(60)))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        try {
            token.sign(signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        this.token = token.serialize();
    }

    public String getToken() {
        return this.token;
    }
}

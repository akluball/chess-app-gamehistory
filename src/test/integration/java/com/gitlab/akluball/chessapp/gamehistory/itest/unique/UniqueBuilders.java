package com.gitlab.akluball.chessapp.gamehistory.itest.unique;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;

import javax.inject.Inject;

public class UniqueBuilders {
    private final UniqueData uniqueData;
    private final Builders builders;

    @Inject
    UniqueBuilders(UniqueData uniqueData, Builders builders) {
        this.uniqueData = uniqueData;
        this.builders = builders;
    }

    public GameHistory.Builder gameHistory() {
        return this.builders.gameHistory()
                .whiteSideUser(this.uniqueData.userId())
                .blackSideUser(this.uniqueData.userId())
                .secondsSinceStart(60)
                .inProgress();
    }
}

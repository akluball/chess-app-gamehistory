package com.gitlab.akluball.chessapp.gamehistory.itest.data;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.IndirectGameRequest;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import java.util.List;
import java.util.stream.Stream;

public class IndirectGameRequestDao {
    private final EntityManager entityManager;

    @Inject
    IndirectGameRequestDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void deleteAll() {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.createNamedQuery(IndirectGameRequest.Query.Name.DELETE_ALL)
                .setFlushMode(FlushModeType.COMMIT)
                .executeUpdate();
        tx.commit();
    }

    public void persist(IndirectGameRequest... indirectGameRequests) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        Stream.of(indirectGameRequests).forEach(this.entityManager::persist);
        tx.commit();
    }

    public void delete(IndirectGameRequest indirectGameRequest) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.remove(indirectGameRequest);
        tx.commit();
    }

    public List<IndirectGameRequest> findByUserId(int userId) {
        return this.entityManager
                .createNamedQuery(IndirectGameRequest.Query.Name.FIND_BY_USER_ID, IndirectGameRequest.class)
                .setParameter(IndirectGameRequest.Query.Param.USER_ID, userId)
                .getResultList();
    }
}

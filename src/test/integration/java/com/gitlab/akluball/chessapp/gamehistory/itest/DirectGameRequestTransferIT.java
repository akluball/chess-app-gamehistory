package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.DirectGameRequestClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.DirectGameRequestDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.DirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.DirectGameRequestDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.LIST_OF_DIRECT_GAME_REQUEST_DTO;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class DirectGameRequestTransferIT {
    private final UniqueData uniqueData;
    private final Builders builders;
    private final DirectGameRequestDao directGameRequestDao;
    private final UserSecurity userSecurity;
    private final DirectGameRequestClient directGameRequestClient;

    @Guicy
    DirectGameRequestTransferIT(UniqueData uniqueData,
            Builders builders,
            DirectGameRequestDao directGameRequestDao,
            UserSecurity userSecurity,
            DirectGameRequestClient directGameRequestClient) {
        this.uniqueData = uniqueData;
        this.builders = builders;
        this.directGameRequestDao = directGameRequestDao;
        this.userSecurity = userSecurity;
        this.directGameRequestClient = directGameRequestClient;
    }

    @Test
    void id() {
        int userId = this.uniqueData.userId();
        DirectGameRequest incoming = this.builders.directGameRequest().to(userId).build();
        this.directGameRequestDao.persist(incoming);
        Response response = this.directGameRequestClient.getIncoming(this.userSecurity.tokenFor(userId));
        DirectGameRequestDto incomingDto = response.readEntity(LIST_OF_DIRECT_GAME_REQUEST_DTO).get(0);
        assertThat(incomingDto.getId()).isEqualTo(incoming.getId());
    }

    @Test
    void requesterId() {
        int userId = this.uniqueData.userId();
        this.directGameRequestDao.persist(this.builders.directGameRequest().from(userId).build());
        Response response = this.directGameRequestClient.getOutgoing(this.userSecurity.tokenFor(userId));
        DirectGameRequestDto incomingDto = response.readEntity(LIST_OF_DIRECT_GAME_REQUEST_DTO).get(0);
        assertThat(incomingDto.getRequesterId()).isEqualTo(userId);
    }

    @Test
    void requesteeId() {
        int userId = this.uniqueData.userId();
        this.directGameRequestDao.persist(this.builders.directGameRequest().to(userId).build());
        Response response = this.directGameRequestClient.getIncoming(this.userSecurity.tokenFor(userId));
        DirectGameRequestDto incomingDto = response.readEntity(LIST_OF_DIRECT_GAME_REQUEST_DTO).get(0);
        assertThat(incomingDto.getRequesteeId()).isEqualTo(userId);
    }

    @Test
    void epochSeconds() {
        int userId = this.uniqueData.userId();
        DirectGameRequest request = this.builders.directGameRequest().from(userId).epochSeconds(1000L).build();
        this.directGameRequestDao.persist(request);
        Response response = this.directGameRequestClient.getOutgoing(this.userSecurity.tokenFor(userId));
        DirectGameRequestDto incomingDto = response.readEntity(LIST_OF_DIRECT_GAME_REQUEST_DTO).get(0);
        assertThat(incomingDto.getEpochSeconds()).isEqualTo(1000L);
    }
}

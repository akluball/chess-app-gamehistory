package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.GameHistoryClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.GameHistoryDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameStatus;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.ResultTag;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.SourceDisambiguation;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.TerminationTag;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.GameHistoryDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.MoveSummaryDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameHistoryTransferIT {
    private final Builders builders;
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameHistoryDao gameHistoryDao;
    private final UserSecurity userSecurity;
    private final GameHistoryClient gameHistoryClient;

    @Inject
    @Guicy
    GameHistoryTransferIT(Builders builders,
            UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameHistoryDao gameHistoryDao,
            UserSecurity userSecurity,
            GameHistoryClient gameHistoryClient) {
        this.builders = builders;
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameHistoryDao = gameHistoryDao;
        this.userSecurity = userSecurity;
        this.gameHistoryClient = gameHistoryClient;
    }

    @Test
    void gameHistoryId() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getId()).isEqualTo(gameHistory.getId());
    }

    @Test
    void whiteSideUserId() {
        int whiteSideUserId = this.uniqueData.userId();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().whiteSideUser(whiteSideUserId).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(whiteSideUserId);
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getWhiteSideUserId()).isEqualTo(whiteSideUserId);
    }

    @Test
    void blackSideUserId() {
        int blackSideUserId = this.uniqueData.userId();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().blackSideUser(blackSideUserId).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(blackSideUserId);
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getBlackSideUserId()).isEqualTo(blackSideUserId);
    }

    @Test
    void millisPerPlayer() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().millisPerPlayer(600_000L).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getMillisPerPlayer()).isEqualTo(600_000L);
    }

    @Test
    void startEpochSeconds() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().startEpochSeconds(111000L).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getStartEpochSeconds()).isEqualTo(111000L);
    }

    @Test
    void whiteWinsRatingAdjustment() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().whiteWinsRatingAdjustment(11).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getWhiteWinsRatingAdjustment()).isEqualTo(11);
    }

    @Test
    void whiteLosesRatingAdjustment() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().whiteLosesRatingAdjustment(-5).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getWhiteLosesRatingAdjustment()).isEqualTo(-5);
    }

    @Test
    void whiteDrawsRatingAdjustment() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().whiteDrawsRatingAdjustment(3).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getWhiteDrawsRatingAdjustment()).isEqualTo(3);
    }

    @Test
    void moveCount() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory()
                .moveSummary(this.builders.moveSummary().build())
                .moveSummary(this.builders.moveSummary().build()).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getMoveCount()).isEqualTo(2);
    }

    @Test
    void moveSummaries() {
        MoveSummary moveSummary = this.builders.moveSummary().build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getMoveSummaries()).hasSize(1);
    }

    @Test
    void moveSummaryPieceLetter() {
        MoveSummary moveSummary = this.builders.moveSummary().pieceLetter("P").build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        MoveSummaryDto moveSummaryDto = response.readEntity(GameHistoryDto.class).getMoveSummaries().get(0);
        assertThat(moveSummaryDto.getPieceLetter()).isEqualTo("P");
    }

    @Test
    void moveSummaryPromotionPieceLetter() {
        MoveSummary moveSummary = this.builders.moveSummary().promotionPieceLetter("Q").build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        MoveSummaryDto moveSummaryDto = response.readEntity(GameHistoryDto.class).getMoveSummaries().get(0);
        assertThat(moveSummaryDto.getPromotionPieceLetter()).isEqualTo("Q");
    }

    @Test
    void moveSummarySource() {
        MoveSummary moveSummary = this.builders.moveSummary().source("e1").build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        MoveSummaryDto moveSummaryDto = response.readEntity(GameHistoryDto.class).getMoveSummaries().get(0);
        assertThat(moveSummaryDto.getSource()).isEqualTo("e1");
    }

    @Test
    void moveSummaryTarget() {
        MoveSummary moveSummary = this.builders.moveSummary().target("b4").build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        MoveSummaryDto moveSummaryDto = response.readEntity(GameHistoryDto.class).getMoveSummaries().get(0);
        assertThat(moveSummaryDto.getTarget()).isEqualTo("b4");
    }

    @Test
    void moveSummaryIsCapture() {
        MoveSummary moveSummary = this.builders.moveSummary().isCapture(true).build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        MoveSummaryDto moveSummaryDto = response.readEntity(GameHistoryDto.class).getMoveSummaries().get(0);
        assertThat(moveSummaryDto.isCapture()).isTrue();
    }

    @Test
    void moveSummarySourceDisambiguation() {
        MoveSummary moveSummary = this.builders.moveSummary().sourceDisambiguation(SourceDisambiguation.RANK).build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        MoveSummaryDto moveSummaryDto = response.readEntity(GameHistoryDto.class).getMoveSummaries().get(0);
        assertThat(moveSummaryDto.getSourceDisambiguation()).isEqualTo(SourceDisambiguation.RANK);
    }

    @Test
    void moveSummaryDurationMillis() {
        MoveSummary moveSummary = this.builders.moveSummary().durationMillis(1000L).build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        MoveSummaryDto moveSummaryDto = response.readEntity(GameHistoryDto.class).getMoveSummaries().get(0);
        assertThat(moveSummaryDto.getDurationMillis()).isEqualTo(1000L);
    }

    @Test
    void moveSummaryGameStatus() {
        MoveSummary moveSummary = this.builders.moveSummary().gameStatus(GameStatus.BLACK_IN_CHECK).build();
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().moveSummary(moveSummary).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        MoveSummaryDto moveSummaryDto = response.readEntity(GameHistoryDto.class).getMoveSummaries().get(0);
        assertThat(moveSummaryDto.getGameStatus()).isEqualTo(GameStatus.BLACK_IN_CHECK);
    }

    @Test
    void resultTag() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().whiteWins().build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getResultTag()).isEqualTo(ResultTag.WHITE_WINS);
    }

    @Test
    void terminationTag() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().terminationTag(TerminationTag.TIME_FORFEIT).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getBlackSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getTerminationTag())
                .isEqualTo(TerminationTag.TIME_FORFEIT);
    }

    @Test
    void endEpochSeconds() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().endEpochSeconds(222000L).build();
        this.gameHistoryDao.persist(gameHistory);
        String token = this.userSecurity.tokenFor(gameHistory.getWhiteSideUserId());
        Response response = this.gameHistoryClient.getById(token, gameHistory);
        assertThat(response.readEntity(GameHistoryDto.class).getEndEpochSeconds()).isEqualTo(222000L);
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.DirectGameRequestClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.DirectGameRequestDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockGameState;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUser;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.DirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.GameHistorySecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.DirectGameRequestDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.GameStateCreateDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.LIST_OF_DIRECT_GAME_REQUEST_DTO;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class DirectGameRequestControllerIT {
    private final UniqueData uniqueData;
    private final Builders builders;
    private final DirectGameRequestDao directGameRequestDao;
    private final MockUser mockUser;
    private final MockGameState mockGameState;
    private final UserSecurity userSecurity;
    private final DirectGameRequestClient directGameRequestClient;

    @Guicy
    DirectGameRequestControllerIT(UniqueData uniqueData,
            Builders builders,
            DirectGameRequestDao directGameRequestDao,
            MockUser mockUser,
            MockGameState mockGameState,
            UserSecurity userSecurity,
            DirectGameRequestClient directGameRequestClient) {
        this.uniqueData = uniqueData;
        this.builders = builders;
        this.directGameRequestDao = directGameRequestDao;
        this.mockUser = mockUser;
        this.mockGameState = mockGameState;
        this.userSecurity = userSecurity;
        this.directGameRequestClient = directGameRequestClient;
    }

    private DirectGameRequest persistedDirect() {
        DirectGameRequest directGameRequest = this.builders.directGameRequest()
                .from(this.uniqueData.userId()).to(this.uniqueData.userId()).millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directGameRequest);
        return directGameRequest;
    }

    private DirectGameRequest unpersistedDirect() {
        DirectGameRequest directRequest = this.persistedDirect();
        this.directGameRequestDao.delete(directRequest);
        return directRequest;
    }

    @Test
    void create() {
        int userId = this.uniqueData.userId();
        int requesteeUserId = this.uniqueData.userId();
        this.mockUser.expectGet(requesteeUserId);
        Response response = this.directGameRequestClient.create(this.userSecurity.tokenFor(userId), requesteeUserId);
        assertThat(response.getStatus()).isEqualTo(204);
        this.mockUser.clearGet(requesteeUserId);
    }

    @Test
    void createNoAuth() {
        Response response = this.directGameRequestClient.create(null, this.uniqueData.userId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createBadRole() {
        String badRoleToken = this.userSecurity.badRoleTokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.create(badRoleToken, this.uniqueData.userId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createBadSignature() {
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.create(badSignatureToken, this.uniqueData.userId());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createNonIntegerRequesteeId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.create(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoRequestee() {
        int userId = this.uniqueData.userId();
        Response response = this.directGameRequestClient.create(this.userSecurity.tokenFor(userId), null);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createRequesteeNotFound() {
        int userId = this.uniqueData.userId();
        int requesteeUserId = this.uniqueData.userId();
        this.mockUser.expectGetNotFound(requesteeUserId);
        Response response = this.directGameRequestClient.create(this.userSecurity.tokenFor(userId), requesteeUserId);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
        this.mockUser.clearGet(requesteeUserId);
    }

    @Test
    void getIncoming() {
        int userId = this.uniqueData.userId();
        DirectGameRequest incoming = this.builders.directGameRequest().to(userId).build();
        DirectGameRequest outgoing = this.builders.directGameRequest().from(userId).build();
        this.directGameRequestDao.persist(incoming, outgoing);
        Response response = this.directGameRequestClient.getIncoming(this.userSecurity.tokenFor(userId));
        assertThat(response.getStatus()).isEqualTo(200);
        List<DirectGameRequestDto> incomingRequests = response.readEntity(LIST_OF_DIRECT_GAME_REQUEST_DTO);
        assertThat(incomingRequests.stream().map(DirectGameRequestDto::getId).collect(Collectors.toList()))
                .containsExactly(incoming.getId());
    }

    @Test
    void getIncomingOrdersByFreshness() {
        int userId = this.uniqueData.userId();
        DirectGameRequest mostRecent = this.builders.directGameRequest().to(userId).epochSeconds(10_000L).build();
        DirectGameRequest leastRecent = this.builders.directGameRequest().to(userId).epochSeconds(4_000L).build();
        DirectGameRequest lessRecent = this.builders.directGameRequest().to(userId).epochSeconds(7_000L).build();
        this.directGameRequestDao.persist(mostRecent, leastRecent, lessRecent);
        Response response = this.directGameRequestClient.getIncoming(this.userSecurity.tokenFor(userId));
        assertThat(response.getStatus()).isEqualTo(200);
        List<DirectGameRequestDto> incomingRequests = response.readEntity(LIST_OF_DIRECT_GAME_REQUEST_DTO);
        assertThat(incomingRequests.stream().map(DirectGameRequestDto::getId).collect(Collectors.toList()))
                .containsExactly(mostRecent.getId(), lessRecent.getId(), leastRecent.getId()).inOrder();
    }

    @Test
    void getIncomingNoAuth() {
        Response response = this.directGameRequestClient.getIncoming(null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getIncomingBadRole() {
        String badRoleToken = this.userSecurity.badRoleTokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.getIncoming(badRoleToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getIncomingBadSignature() {
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.getIncoming(badSignatureToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getOutgoing() {
        int userId = this.uniqueData.userId();
        DirectGameRequest outgoing = this.builders.directGameRequest().from(userId).build();
        DirectGameRequest incoming = this.builders.directGameRequest().to(userId).build();
        this.directGameRequestDao.persist(outgoing, incoming);
        Response response = this.directGameRequestClient.getOutgoing(this.userSecurity.tokenFor(userId));
        assertThat(response.getStatus()).isEqualTo(200);
        List<DirectGameRequestDto> outgoingRequests = response.readEntity(LIST_OF_DIRECT_GAME_REQUEST_DTO);
        assertThat(outgoingRequests.stream().map(DirectGameRequestDto::getId).collect(Collectors.toList()))
                .containsExactly(outgoing.getId());
    }

    @Test
    void getOutgoingOrdersByFreshness() {
        int userId = this.uniqueData.userId();
        DirectGameRequest mostRecent = this.builders.directGameRequest().from(userId).epochSeconds(10_000L).build();
        DirectGameRequest leastRecent = this.builders.directGameRequest().from(userId).epochSeconds(4_000L).build();
        DirectGameRequest lessRecent = this.builders.directGameRequest().from(userId).epochSeconds(7_000L).build();
        this.directGameRequestDao.persist(mostRecent, leastRecent, lessRecent);
        Response response = this.directGameRequestClient.getOutgoing(this.userSecurity.tokenFor(userId));
        assertThat(response.getStatus()).isEqualTo(200);
        List<DirectGameRequestDto> outgoingRequests = response.readEntity(LIST_OF_DIRECT_GAME_REQUEST_DTO);
        assertThat(outgoingRequests.stream().map(DirectGameRequestDto::getId).collect(Collectors.toList()))
                .containsExactly(mostRecent.getId(), lessRecent.getId(), leastRecent.getId()).inOrder();
    }

    @Test
    void getOutgoingNoAuth() {
        Response response = this.directGameRequestClient.getOutgoing(null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getOutgoingBadRole() {
        String badRoleToken = this.userSecurity.badRoleTokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.getOutgoing(badRoleToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getOutgoingBadSignature() {
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.getOutgoing(badSignatureToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void decline() {
        int userId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().to(userId).build();
        this.directGameRequestDao.persist(directRequest);
        Response response = this.directGameRequestClient.decline(this.userSecurity.tokenFor(userId), directRequest);
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    void declineNoAuth() {
        DirectGameRequest directRequest = this.builders.directGameRequest().build();
        this.directGameRequestDao.persist(directRequest);
        Response response = this.directGameRequestClient.decline(null, directRequest);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void declineNonIntegerRequestId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.decline(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void declineNotRequestee() {
        DirectGameRequest directRequest = this.persistedDirect();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.decline(token, directRequest);
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void declineRequestNotFound() {
        DirectGameRequest unpersistedDirect = this.unpersistedDirect();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.decline(token, unpersistedDirect);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void accept() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(200);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptPutGameState() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(200);
        this.mockGameState.verifyPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptPutGameStateFail() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPutFail(createDto);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(503);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    @Guicy
    void acceptGetUserAuth(GameHistorySecurity gameHistorySecurity) {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(200);
        SignedJWT gameHistoryToken = this.mockUser.retrieveGetToken(requesterId);
        assertThat(gameHistorySecurity.isValidSignature(gameHistoryToken)).isTrue();
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    @Guicy
    void acceptPutGameStateAuth(GameHistorySecurity gameHistorySecurity) {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(200);
        SignedJWT gameHistoryToken = this.mockGameState.retrievePutToken(createDto);
        assertThat(gameHistorySecurity.isValidSignature(gameHistoryToken)).isTrue();
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptNoAuth() {
        Response response = this.directGameRequestClient.accept(null, this.persistedDirect());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void acceptNonIntegerRequestId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.accept(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void acceptNotRequestee() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.accept(token, this.persistedDirect());
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void acceptNotFound() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.accept(token, this.unpersistedDirect());
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void acceptRequesterNotFound() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId).build();
        this.directGameRequestDao.persist(directRequest);
        mockUser.expectGetNotFound(requesterId);
        mockUser.expectGet(requesteeId);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
        mockUser.clearGet(requesterId);
        mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptRequesteeNotFound() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId).build();
        this.directGameRequestDao.persist(directRequest);
        mockUser.expectGet(requesterId);
        mockUser.expectGetNotFound(requesteeId);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
        mockUser.clearGet(requesterId);
        mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptRequesterMissingId() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId, this.builders.userDto().rating(0).build());
        this.mockUser.expectGet(requesteeId);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptRequesterMissingRating() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId, this.builders.userDto().id(requesterId).build());
        this.mockUser.expectGet(requesteeId);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptRequesteeMissingId() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId, this.builders.userDto().rating(0).build());
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptMissingMillisPerPlayer() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(null).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void acceptNegativeMillisPerPlayer() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(-600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        Response response = this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void cancel() {
        int userId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(userId).build();
        this.directGameRequestDao.persist(directRequest);
        Response response = this.directGameRequestClient.cancel(this.userSecurity.tokenFor(userId), directRequest);
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    void cancelNoAuth() {
        Response response = this.directGameRequestClient.cancel(null, this.persistedDirect());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void cancelNonIntegerRequestId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.cancel(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void cancelNotRequester() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.cancel(token, this.persistedDirect());
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void cancelNotFound() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.directGameRequestClient.cancel(token, this.unpersistedDirect());
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }
}

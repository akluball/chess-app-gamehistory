package com.gitlab.akluball.chessapp.gamehistory.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderColumn;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = GameHistory.Query.Name.FIND_BY_PARTICIPANTS, query = GameHistory.Query.FIND_BY_PARTICIPANTS)
})
public class GameHistory {
    @Id
    @GeneratedValue
    private int id;
    private int whiteSideUserId;
    private int blackSideUserId;
    private int whiteWinsRatingAdjustment;
    private int whiteLosesRatingAdjustment;
    private int whiteDrawsRatingAdjustment;
    private long startEpochSeconds;
    private long endEpochSeconds;
    @ElementCollection
    @OrderColumn
    private List<MoveSummary> moveSummaries = new ArrayList<>();
    private long millisPerPlayer;
    @Enumerated(EnumType.STRING)
    private ResultTag resultTag;
    @Enumerated(EnumType.STRING)
    private TerminationTag terminationTag;

    public int getId() {
        return this.id;
    }

    public int getWhiteSideUserId() {
        return this.whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return this.blackSideUserId;
    }

    public long getEndEpochSeconds() {
        return endEpochSeconds;
    }

    public int getWhiteWinsRatingAdjustment() {
        return whiteWinsRatingAdjustment;
    }

    public int getWhiteLosesRatingAdjustment() {
        return whiteLosesRatingAdjustment;
    }

    public int getWhiteDrawsRatingAdjustment() {
        return whiteDrawsRatingAdjustment;
    }

    public ResultTag getResultTag() {
        return resultTag;
    }

    public TerminationTag getTerminationTag() {
        return terminationTag;
    }

    public List<MoveSummary> getMoveSummaries() {
        return this.moveSummaries;
    }

    public long getMillisPerPlayer() {
        return millisPerPlayer;
    }

    public EndGameUserUpdateDto toEndGameUserUpdateDto() {
        return this.new EndGameUserUpdateDto();
    }

    public static class Builder {
        private final GameHistory gameHistory;

        Builder() {
            this.gameHistory = new GameHistory();
        }

        public Builder whiteSideUser(int whiteSideUserId) {
            this.gameHistory.whiteSideUserId = whiteSideUserId;
            return this;
        }

        public Builder blackSideUser(int blackSideUserId) {
            this.gameHistory.blackSideUserId = blackSideUserId;
            return this;
        }

        public Builder millisPerPlayer(long millisPerPlayer) {
            this.gameHistory.millisPerPlayer = millisPerPlayer;
            return this;
        }

        public Builder startEpochSeconds(long epochSeconds) {
            this.gameHistory.startEpochSeconds = epochSeconds;
            return this;
        }

        public Builder secondsSinceStart(long seconds) {
            this.gameHistory.startEpochSeconds = Instant.now().minusSeconds(seconds).getEpochSecond();
            return this;
        }

        public Builder endEpochSeconds(long endEpochSeconds) {
            this.gameHistory.endEpochSeconds = endEpochSeconds;
            return this;
        }

        public Builder whiteWinsRatingAdjustment(int adjustment) {
            this.gameHistory.whiteWinsRatingAdjustment = adjustment;
            return this;
        }

        public Builder whiteLosesRatingAdjustment(int adjustment) {
            this.gameHistory.whiteLosesRatingAdjustment = adjustment;
            return this;
        }

        public Builder whiteDrawsRatingAdjustment(int adjustment) {
            this.gameHistory.whiteDrawsRatingAdjustment = adjustment;
            return this;
        }

        public Builder inProgress() {
            this.gameHistory.resultTag = ResultTag.IN_PROGRESS;
            this.gameHistory.terminationTag = TerminationTag.UNTERMINATED;
            return this;
        }

        public Builder whiteWins() {
            this.gameHistory.resultTag = ResultTag.WHITE_WINS;
            this.gameHistory.terminationTag = TerminationTag.NORMAL;
            return this;
        }

        public Builder blackWins() {
            this.gameHistory.resultTag = ResultTag.BLACK_WINS;
            this.gameHistory.terminationTag = TerminationTag.NORMAL;
            return this;
        }

        public Builder draw() {
            this.gameHistory.resultTag = ResultTag.DRAW;
            this.gameHistory.terminationTag = TerminationTag.NORMAL;
            return this;
        }

        public Builder terminationTag(TerminationTag terminationTag) {
            this.gameHistory.terminationTag = terminationTag;
            return this;
        }

        public Builder moveSummary(MoveSummary moveSummary) {
            this.gameHistory.moveSummaries.add(moveSummary);
            return this;
        }

        public GameHistory build() {
            return this.gameHistory;
        }
    }

    public static class Query {
        public static final String FIND_BY_PARTICIPANTS = "SELECT gameHistory FROM GameHistory AS gameHistory"
                + " WHERE (gameHistory.whiteSideUserId=:" + Param.USER_ONE_ID
                + " AND gameHistory.blackSideUserId=:" + Param.USER_TWO_ID + ")"
                + " OR (gameHistory.blackSideUserId=:" + Param.USER_ONE_ID
                + " AND gameHistory.whiteSideUserId=:" + Param.USER_TWO_ID + ")";

        public static class Name {
            public static final String FIND_BY_PARTICIPANTS = "find-by-participants";
        }

        public static class Param {
            public static final String USER_ONE_ID = "user_one_id";
            public static final String USER_TWO_ID = "user_two_id";
        }
    }

    public class EndGameUserUpdateDto {
        private final int whiteSideUserId;
        private final int blackSideUserId;

        private EndGameUserUpdateDto() {
            GameHistory gameHistory = GameHistory.this;
            this.whiteSideUserId = gameHistory.whiteSideUserId;
            this.blackSideUserId = gameHistory.blackSideUserId;
        }
    }
}

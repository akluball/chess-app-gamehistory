package com.gitlab.akluball.chessapp.gamehistory.itest.client;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.EndGameDto;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.gamehistory.itest.security.SecurityUtil.bearer;

public class GameHistoryClient {
    private final WebTarget target;

    @Inject
    GameHistoryClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target("gamehistory");
    }

    public Response getById(String token, String gameHistoryId) {
        Invocation.Builder invocationBuilder = this.target.path(gameHistoryId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response getById(String token, GameHistory gameHistory) {
        return this.getById(token, "" + gameHistory.getId());
    }

    public Response getByParticipant(String token, String userId, String isCompleted) {
        WebTarget target = this.target;
        if (Objects.nonNull(userId)) {
            target = target.queryParam("userId", userId);
        }
        if (Objects.nonNull(isCompleted)) {
            target = target.queryParam("isCompleted", isCompleted);
        }
        Invocation.Builder invocationBuilder = target.request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response getByParticipant(String token, Integer userId, Boolean isCompleted) {
        String userIdAsString = Objects.nonNull(userId) ? userId.toString() : null;
        String isCompletedAsString = Objects.nonNull(isCompleted) ? isCompleted.toString() : null;
        return this.getByParticipant(token, userIdAsString, isCompletedAsString);
    }

    public Response endGame(String token, String gameHistoryId, EndGameDto endGameDto) {
        Invocation.Builder invocationBuilder = this.target.path(gameHistoryId).path("end").request();
        bearer(invocationBuilder, token);
        return invocationBuilder.post(Entity.json(Objects.nonNull(endGameDto) ? endGameDto : "null"));
    }

    public Response endGame(String token, GameHistory gameHistory, EndGameDto endGameDto) {
        return this.endGame(token, "" + gameHistory.getId(), endGameDto);
    }
}

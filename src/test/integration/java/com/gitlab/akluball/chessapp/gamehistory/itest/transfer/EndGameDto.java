package com.gitlab.akluball.chessapp.gamehistory.itest.transfer;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.ResultTag;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.TerminationTag;

import java.util.ArrayList;
import java.util.List;

public class EndGameDto {
    private ResultTag resultTag;
    private TerminationTag terminationTag;
    private List<MoveSummary> moveSummaries;

    public static class Builder {
        private final EndGameDto endGameDto;

        public Builder() {
            this.endGameDto = new EndGameDto();
            this.endGameDto.moveSummaries = new ArrayList<>();
        }

        public Builder resultTag(ResultTag resultTag) {
            this.endGameDto.resultTag = resultTag;
            return this;
        }

        public Builder terminationTag(TerminationTag terminationTag) {
            this.endGameDto.terminationTag = terminationTag;
            return this;
        }

        public Builder moveSummaries(List<MoveSummary> moveSummaries) {
            this.endGameDto.moveSummaries = moveSummaries;
            return this;
        }

        public Builder moveSummary(MoveSummary moveSummary) {
            this.endGameDto.moveSummaries.add(moveSummary);
            return this;
        }

        public EndGameDto build() {
            return this.endGameDto;
        }
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Properties;

import static com.gitlab.akluball.chessapp.gamehistory.itest.hook.DeployUtil.*;
import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.minutesToMillis;

@Singleton
public class TestConfig {
    public static final String GAMEHISTORY_PUBLICKEY_PROP = "chessapp.gamehistory.publickey";
    public static final String USER_PRIVATEKEY_PROP = "chessapp.user.privatekey";
    public static final String GAMESTATE_PRIVATEKEY_PROP = "chessapp.gamestate.privatekey";
    public static final String MESSAGING_PRIVATEKEY_PROP = "chessapp.messaging.privatekey";

    public static final int ELO_RATING_K_FACTOR = 32;
    public static final long DEFAULT_MILLIS_PER_PLAYER = minutesToMillis(10);

    private final String gameHistoryUri;
    private final String dbUri;
    private final String dbUser;
    private final String dbPassword;
    private final String gameStateIp;
    private final String userIp;
    private final RSAPublicKey gameHistoryPublicKey;
    private final PrivateKey userPrivateKey;
    private final PrivateKey gameStatePrivateKey;
    private final PrivateKey messagingPrivateKey;

    public TestConfig() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        this.gameHistoryUri = getGameHistoryUri();
        this.dbUri = getDbUri();
        this.dbUser = DB_USER;
        this.dbPassword = DB_PASSWORD;
        this.gameStateIp = getGameStateIp();
        this.userIp = getUserIp();
        try (InputStream configStream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("gamehistory-test.properties")) {
            Properties config = new Properties();
            config.load(configStream);
            Base64.Decoder decoder = Base64.getDecoder();
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] gameHistoryPublicKeyBytes = decoder.decode(config.getProperty(GAMEHISTORY_PUBLICKEY_PROP));
            this.gameHistoryPublicKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(
                    gameHistoryPublicKeyBytes));
            byte[] userPrivateKeyBytes = decoder.decode(config.getProperty(USER_PRIVATEKEY_PROP));
            this.userPrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(userPrivateKeyBytes));
            byte[] gameStatePrivateKeyBytes = decoder.decode(config.getProperty(GAMESTATE_PRIVATEKEY_PROP));
            this.gameStatePrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(gameStatePrivateKeyBytes));
            byte[] messagingPrivateKeyBytes = decoder.decode(config.getProperty(MESSAGING_PRIVATEKEY_PROP));
            this.messagingPrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(messagingPrivateKeyBytes));
        }
    }

    public String gameHistoryUri() {
        return this.gameHistoryUri;
    }

    public String dbUri() {
        return this.dbUri;
    }

    public String dbUser() {
        return this.dbUser;
    }

    public String dbPassword() {
        return this.dbPassword;
    }

    public String gameStateHost() {
        return this.gameStateIp;
    }

    public int gameStatePort() {
        return 1080;
    }

    public String userHost() {
        return this.userIp;
    }

    public int userPort() {
        return 1080;
    }

    public RSAPublicKey gameHistoryPublicKey() {
        return this.gameHistoryPublicKey;
    }

    public PrivateKey userPrivateKey() {
        return this.userPrivateKey;
    }

    public PrivateKey gameStatePrivateKey() {
        return this.gameStatePrivateKey;
    }

    public PrivateKey messagingPrivateKey() {
        return this.messagingPrivateKey;
    }

    public int eloRatingKFactor() {
        return ELO_RATING_K_FACTOR;
    }

    public long defaultMillisPerPlayer() {
        return DEFAULT_MILLIS_PER_PLAYER;
    }
}

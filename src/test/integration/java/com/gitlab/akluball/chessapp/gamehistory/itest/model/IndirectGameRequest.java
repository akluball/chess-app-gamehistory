package com.gitlab.akluball.chessapp.gamehistory.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = IndirectGameRequest.Query.Name.DELETE_ALL, query = IndirectGameRequest.Query.DELETE_ALL),
        @NamedQuery(name = IndirectGameRequest.Query.Name.FIND_BY_USER_ID,
                query = IndirectGameRequest.Query.FIND_BY_USER_ID),
})
public class IndirectGameRequest {
    @Id
    @GeneratedValue
    private Integer id;
    private Integer requesterId;
    private Long millisPerPlayer;

    public Integer getId() {
        return id;
    }

    public Long getMillisPerPlayer() {
        return millisPerPlayer;
    }

    public static class Builder {
        private final IndirectGameRequest indirectGameRequest;

        Builder() {
            this.indirectGameRequest = new IndirectGameRequest();
        }

        public Builder requester(Integer requesterId) {
            this.indirectGameRequest.requesterId = requesterId;
            return this;
        }

        public Builder millisPerPlayer(Long millisPerPlayer) {
            this.indirectGameRequest.millisPerPlayer = millisPerPlayer;
            return this;
        }

        public IndirectGameRequest build() {
            return this.indirectGameRequest;
        }
    }

    public static class Query {
        public static final String DELETE_ALL = "DELETE FROM IndirectGameRequest";
        public static final String FIND_BY_USER_ID = "SELECT req FROM IndirectGameRequest AS req WHERE req.requesterId=:" + Param.USER_ID;

        public static class Name {
            public static final String DELETE_ALL = "delete-all";
            public static final String FIND_BY_USER_ID = "find-by-user-id";
        }

        public static class Param {
            public static final String USER_ID = "user_id";
        }
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest.util;

import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.DirectGameRequestDto;
import com.google.common.collect.Range;

import javax.ws.rs.core.GenericType;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.List;
import java.util.stream.Collectors;

public class Util {
    public static final GenericType<List<DirectGameRequestDto>> LIST_OF_DIRECT_GAME_REQUEST_DTO
            = new GenericType<List<DirectGameRequestDto>>() {};

    public static RuntimeException asRuntime(Throwable throwable) {
        return new RuntimeException(throwable);
    }

    public static void sleepMillis(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw asRuntime(e);
        }
    }

    @SafeVarargs
    public static <E> E[] concat(Class<E> klass, E[]... arraysToConcat) {
        int concatLength = 0;
        for (E[] arrayToConcat : arraysToConcat) {
            concatLength += arrayToConcat.length;
        }
        @SuppressWarnings("unchecked")
        E[] concat = (E[]) Array.newInstance(klass, concatLength);
        int concatIndex = 0;
        for (E[] arrayToConcat : arraysToConcat) {
            for (E element : arrayToConcat) {
                concat[concatIndex] = element;
                concatIndex++;
            }
        }
        return concat;
    }

    public static String inputStreamToString(InputStream inputStream) {
        return new BufferedReader(new InputStreamReader(inputStream))
                .lines()
                .collect(Collectors.joining(System.lineSeparator()));
    }

    public static Range<Long> bufferBelow4(long upperLimit) {
        return Range.closed(upperLimit - 4, upperLimit);
    }

    public static long minutesToMillis(long minutes) {
        return minutes * 60_000;
    }
}

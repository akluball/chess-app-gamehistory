package com.gitlab.akluball.chessapp.gamehistory.itest.hook;

import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockGameState;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUser;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.ParameterResolverModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import static com.gitlab.akluball.chessapp.gamehistory.itest.hook.DeployUtil.*;

public class Before {
    public static void main(String[] args) {
        String gameHistoryWar = System.getenv("CHESSAPP_GAMEHISTORY_WAR");
        copyToContainer(gameHistoryWar, PAYARA_CONTAINER, CONTAINER_GAMEHISTORY_WAR);
        containerAsadmin("redeploy", "--name", GAMEHISTORY_DEPLOY_NAME, CONTAINER_GAMEHISTORY_WAR);
        Injector injector = Guice.createInjector(new ParameterResolverModule());
        EntityManager entityManager = injector.getInstance(EntityManager.class);
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.flush();
        tx.commit();
        injector.getInstance(MockGameState.class).reset();
        injector.getInstance(MockUser.class).reset();
    }
}

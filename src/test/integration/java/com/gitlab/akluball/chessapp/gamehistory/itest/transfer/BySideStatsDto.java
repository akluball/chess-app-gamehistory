package com.gitlab.akluball.chessapp.gamehistory.itest.transfer;

public class BySideStatsDto {
    private StatsDto whiteSideStats;
    private StatsDto blackSideStats;

    public int getWhiteSideWinCount() {
        return this.whiteSideStats.winCount;
    }

    public int getWhiteSideLossCount() {
        return this.whiteSideStats.lossCount;
    }

    public int getWhiteSideDrawCount() {
        return this.whiteSideStats.drawCount;
    }

    public int getBlackSideWinCount() {
        return this.blackSideStats.winCount;
    }

    public int getBlackSideLossCount() {
        return this.blackSideStats.lossCount;
    }

    public int getBlackSideDrawCount() {
        return this.blackSideStats.drawCount;
    }

    public static class StatsDto {
        private int winCount;
        private int lossCount;
        private int drawCount;
    }
}

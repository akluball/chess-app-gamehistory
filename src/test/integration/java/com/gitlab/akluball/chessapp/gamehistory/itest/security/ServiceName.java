package com.gitlab.akluball.chessapp.gamehistory.itest.security;

public enum ServiceName {
    GAMEHISTORY("gamehistory"), GAMESTATE("gamestate"), MESSAGING("messaging");

    private final String asString;

    ServiceName(String asString) {
        this.asString = asString;
    }

    public String asString() {
        return this.asString;
    }
}

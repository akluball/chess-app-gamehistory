package com.gitlab.akluball.chessapp.gamehistory.itest.security;

import com.nimbusds.jwt.SignedJWT;

import javax.ws.rs.client.Invocation;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.text.ParseException;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.asRuntime;

public class SecurityUtil {
    public static SignedJWT parseSignedToken(String serializedJwt) {
        try {
            return SignedJWT.parse(serializedJwt);
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }

    public static String extractSubject(SignedJWT token) {
        try {
            return token.getJWTClaimsSet().getSubject();
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }

    public static Set<BearerRole> extractBearerRoles(SignedJWT token) {
        try {
            return token.getJWTClaimsSet().getStringListClaim(BearerRole.CLAIM_NAME)
                    .stream()
                    .map(BearerRole::fromString)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }

    public static PrivateKey rsaPrivateKey() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            return keyPairGenerator.generateKeyPair().getPrivate();
        } catch (NoSuchAlgorithmException e) {
            throw asRuntime(e);
        }
    }

    public static void bearer(Invocation.Builder invocationBuilder, String token) {
        if (token != null) {
            invocationBuilder.header("Authorization", String.format("Bearer %s", token));
        }
    }
}

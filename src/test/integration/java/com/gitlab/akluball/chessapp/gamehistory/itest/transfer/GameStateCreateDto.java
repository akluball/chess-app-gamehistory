package com.gitlab.akluball.chessapp.gamehistory.itest.transfer;

public class GameStateCreateDto {
    private int whiteSideUserId;
    private int blackSideUserId;

    public static class Builder {
        private final GameStateCreateDto gameStateCreateDto;

        Builder() {
            this.gameStateCreateDto = new GameStateCreateDto();
        }

        public Builder whiteSideUserId(int userId) {
            this.gameStateCreateDto.whiteSideUserId = userId;
            return this;
        }

        public Builder blackSideUserId(int userId) {
            this.gameStateCreateDto.blackSideUserId = userId;
            return this;
        }

        public GameStateCreateDto build() {
            return this.gameStateCreateDto;
        }
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest.transfer;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.ResultTag;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.TerminationTag;

import java.util.List;

public class GameHistoryDto {
    private int id;
    private int whiteSideUserId;
    private int blackSideUserId;
    private long startEpochSeconds;
    private long endEpochSeconds;
    private long millisPerPlayer;
    private int whiteWinsRatingAdjustment;
    private int whiteLosesRatingAdjustment;
    private int whiteDrawsRatingAdjustment;
    private int moveCount;
    private List<MoveSummaryDto> moveSummaries;
    private ResultTag resultTag;
    private TerminationTag terminationTag;

    public int getId() {
        return id;
    }

    public int getWhiteSideUserId() {
        return whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return blackSideUserId;
    }

    public long getStartEpochSeconds() {
        return this.startEpochSeconds;
    }

    public long getEndEpochSeconds() {
        return endEpochSeconds;
    }

    public long getMillisPerPlayer() {
        return millisPerPlayer;
    }

    public int getWhiteWinsRatingAdjustment() {
        return whiteWinsRatingAdjustment;
    }

    public int getWhiteLosesRatingAdjustment() {
        return whiteLosesRatingAdjustment;
    }

    public int getWhiteDrawsRatingAdjustment() {
        return whiteDrawsRatingAdjustment;
    }

    public int getMoveCount() {
        return moveCount;
    }

    public List<MoveSummaryDto> getMoveSummaries() {
        return moveSummaries;
    }

    public ResultTag getResultTag() {
        return resultTag;
    }

    public TerminationTag getTerminationTag() {
        return terminationTag;
    }
}

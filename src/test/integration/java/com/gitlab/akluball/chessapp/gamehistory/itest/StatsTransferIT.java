package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.StatsClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.GameHistoryDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.BySideStatsDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class StatsTransferIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameHistoryDao gameHistoryDao;
    private final UserSecurity userSecurity;
    private final StatsClient statsClient;

    @Guicy
    StatsTransferIT(UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameHistoryDao gameHistoryDao,
            UserSecurity userSecurity,
            StatsClient statsClient) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameHistoryDao = gameHistoryDao;
        this.userSecurity = userSecurity;
        this.statsClient = statsClient;
    }

    private String arbitraryUserToken() {
        return this.userSecurity.tokenFor(this.uniqueData.userId());
    }

    @Test
    void userStatsWhiteSideWinCount() {
        int userId = this.uniqueData.userId();
        GameHistory firstWhiteWin = this.uniqueBuilders.gameHistory().whiteSideUser(userId).whiteWins().build();
        GameHistory secondWhiteWin = this.uniqueBuilders.gameHistory().whiteSideUser(userId).whiteWins().build();
        GameHistory blackWin = this.uniqueBuilders.gameHistory().blackSideUser(userId).blackWins().build();
        GameHistory whiteDraw = this.uniqueBuilders.gameHistory().whiteSideUser(userId).draw().build();
        this.gameHistoryDao.persist(firstWhiteWin, secondWhiteWin, blackWin, whiteDraw);
        Response response = this.statsClient.getStats(this.arbitraryUserToken(), userId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getWhiteSideWinCount()).isEqualTo(2);
    }

    @Test
    void userStatsWhiteSideLossCount() {
        int userId = this.uniqueData.userId();
        GameHistory blackLoss = this.uniqueBuilders.gameHistory().blackSideUser(userId).whiteWins().build();
        GameHistory whiteLoss = this.uniqueBuilders.gameHistory().whiteSideUser(userId).blackWins().build();
        GameHistory whiteDraw = this.uniqueBuilders.gameHistory().whiteSideUser(userId).draw().build();
        this.gameHistoryDao.persist(blackLoss, whiteLoss, whiteDraw);
        Response response = this.statsClient.getStats(this.arbitraryUserToken(), userId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getWhiteSideLossCount()).isEqualTo(1);
    }

    @Test
    void userStatsWhiteSideDrawCount() {
        int userId = this.uniqueData.userId();
        GameHistory blackDraw = this.uniqueBuilders.gameHistory().blackSideUser(userId).draw().build();
        GameHistory whiteLoss = this.uniqueBuilders.gameHistory().whiteSideUser(userId).blackWins().build();
        GameHistory firstWhiteDraw = this.uniqueBuilders.gameHistory().whiteSideUser(userId).draw().build();
        GameHistory secondWhiteDraw = this.uniqueBuilders.gameHistory().whiteSideUser(userId).draw().build();
        this.gameHistoryDao.persist(blackDraw, whiteLoss, firstWhiteDraw, secondWhiteDraw);
        Response response = this.statsClient.getStats(this.arbitraryUserToken(), userId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getWhiteSideDrawCount()).isEqualTo(2);
    }

    @Test
    void userStatsBlackSideWinCount() {
        int userId = this.uniqueData.userId();
        GameHistory blackWin = this.uniqueBuilders.gameHistory().blackSideUser(userId).blackWins().build();
        GameHistory whiteWin = this.uniqueBuilders.gameHistory().whiteSideUser(userId).whiteWins().build();
        GameHistory blackDraw = this.uniqueBuilders.gameHistory().blackSideUser(userId).draw().build();
        this.gameHistoryDao.persist(blackWin, whiteWin, blackDraw);
        Response response = this.statsClient.getStats(this.arbitraryUserToken(), userId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getBlackSideWinCount()).isEqualTo(1);
    }

    @Test
    void userStatsBlackSideLossCount() {
        int userId = this.uniqueData.userId();
        GameHistory firstBlackLoss = this.uniqueBuilders.gameHistory().blackSideUser(userId).whiteWins().build();
        GameHistory secondBlackLoss = this.uniqueBuilders.gameHistory().blackSideUser(userId).whiteWins().build();
        GameHistory whiteLoss = this.uniqueBuilders.gameHistory().whiteSideUser(userId).blackWins().build();
        GameHistory blackDraw = this.uniqueBuilders.gameHistory().blackSideUser(userId).draw().build();
        this.gameHistoryDao.persist(firstBlackLoss, secondBlackLoss, whiteLoss, blackDraw);
        Response response = this.statsClient.getStats(this.arbitraryUserToken(), userId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getBlackSideLossCount()).isEqualTo(2);
    }

    @Test
    void userStatsBlackSideDrawCount() {
        int userId = this.uniqueData.userId();
        GameHistory blackDraw = this.uniqueBuilders.gameHistory().blackSideUser(userId).draw().build();
        GameHistory blackLoss = this.uniqueBuilders.gameHistory().blackSideUser(userId).whiteWins().build();
        GameHistory whiteDraw = this.uniqueBuilders.gameHistory().whiteSideUser(userId).draw().build();
        this.gameHistoryDao.persist(blackDraw, blackLoss, whiteDraw);
        Response response = this.statsClient.getStats(this.arbitraryUserToken(), userId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getBlackSideDrawCount()).isEqualTo(1);
    }

    private GameHistory.Builder between(int whiteSideUserId, int blackSideUserId) {
        return this.uniqueBuilders.gameHistory()
                .whiteSideUser(whiteSideUserId).blackSideUser(blackSideUserId);
    }

    private GameHistory whiteWins(int whiteSideUserId, int blackSideUserId) {
        return this.between(whiteSideUserId, blackSideUserId).whiteWins().build();
    }

    private GameHistory blackWins(int whiteSideUserId, int blackSideUserId) {
        return this.between(whiteSideUserId, blackSideUserId).blackWins().build();
    }

    private GameHistory draw(int whiteSideUserId, int blackSideUserId) {
        return this.between(whiteSideUserId, blackSideUserId).draw().build();
    }

    @Test
    void versusStatsWhiteSideWinsCount() {
        int userId = this.uniqueData.userId();
        int opponentUserId = this.uniqueData.userId();
        GameHistory whiteVersusWin = this.whiteWins(userId, opponentUserId);
        GameHistory whiteNonVersusWin = this.whiteWins(userId, this.uniqueData.userId());
        GameHistory blackVersusWin = this.blackWins(opponentUserId, userId);
        GameHistory whiteVersusDraw = this.draw(userId, opponentUserId);
        this.gameHistoryDao.persist(whiteVersusWin, whiteNonVersusWin, blackVersusWin, whiteVersusDraw);
        Response response = this.statsClient.getVersusStats(this.userSecurity.tokenFor(userId), opponentUserId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getWhiteSideWinCount()).isEqualTo(1);
    }

    @Test
    void versusStatsWhiteSideLossCount() {
        int userId = this.uniqueData.userId();
        int opponentUserId = this.uniqueData.userId();
        GameHistory firstWhiteVersusLoss = this.blackWins(userId, opponentUserId);
        GameHistory whiteNonVersusLoss = this.blackWins(this.uniqueData.userId(), userId);
        GameHistory secondWhiteVersusLoss = this.blackWins(userId, opponentUserId);
        GameHistory blackVersusLoss = this.whiteWins(opponentUserId, userId);
        this.gameHistoryDao.persist(firstWhiteVersusLoss, whiteNonVersusLoss, secondWhiteVersusLoss, blackVersusLoss);
        Response response = this.statsClient.getVersusStats(this.userSecurity.tokenFor(userId), opponentUserId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getWhiteSideLossCount()).isEqualTo(2);
    }

    @Test
    void versusStatsWhiteSideDrawCount() {
        int userId = this.uniqueData.userId();
        int opponentUserId = this.uniqueData.userId();
        GameHistory whiteVersusDraw = this.draw(userId, opponentUserId);
        GameHistory blackVersusDraw = this.draw(opponentUserId, userId);
        GameHistory nonVersusDraw = this.blackWins(userId, this.uniqueData.userId());
        this.gameHistoryDao.persist(whiteVersusDraw, blackVersusDraw, nonVersusDraw);
        Response response = this.statsClient.getVersusStats(this.userSecurity.tokenFor(userId), opponentUserId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getWhiteSideDrawCount()).isEqualTo(1);
    }

    @Test
    void versusStatsBlackSideWinCount() {
        int userId = this.uniqueData.userId();
        int opponentUserId = this.uniqueData.userId();
        GameHistory firstBlackVersusWin = this.blackWins(opponentUserId, userId);
        GameHistory blackNonVersusWin = this.blackWins(this.uniqueData.userId(), userId);
        GameHistory secondBlackVersusWin = this.blackWins(opponentUserId, userId);
        GameHistory whiteVersusDraw = this.whiteWins(userId, opponentUserId);
        this.gameHistoryDao.persist(firstBlackVersusWin, blackNonVersusWin, secondBlackVersusWin, whiteVersusDraw);
        Response response = this.statsClient.getVersusStats(this.userSecurity.tokenFor(userId), opponentUserId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getBlackSideWinCount()).isEqualTo(2);
    }

    @Test
    void versusStatsBlackSideLossCount() {
        int userId = this.uniqueData.userId();
        int opponentUserId = this.uniqueData.userId();
        GameHistory blackVersusLoss = this.whiteWins(opponentUserId, userId);
        GameHistory blackNonVersusLoss = this.whiteWins(this.uniqueData.userId(), userId);
        GameHistory whiteVersusLoss = this.blackWins(userId, opponentUserId);
        GameHistory blackVersusWin = this.blackWins(opponentUserId, userId);
        this.gameHistoryDao.persist(blackVersusLoss, blackNonVersusLoss, whiteVersusLoss, blackVersusWin);
        Response response = this.statsClient.getVersusStats(this.userSecurity.tokenFor(userId), opponentUserId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getBlackSideLossCount()).isEqualTo(1);
    }

    @Test
    void versusStatsBlackSideDrawCount() {
        int userId = this.uniqueData.userId();
        int opponentUserId = this.uniqueData.userId();
        GameHistory blackVersusDraw = this.draw(opponentUserId, userId);
        GameHistory whiteVersusDraw = this.draw(userId, opponentUserId);
        GameHistory blackNonVersusDraw = this.draw(this.uniqueData.userId(), userId);
        this.gameHistoryDao.persist(blackVersusDraw, whiteVersusDraw, blackNonVersusDraw);
        Response response = this.statsClient.getVersusStats(this.userSecurity.tokenFor(userId), opponentUserId);
        BySideStatsDto bySideStatsDto = response.readEntity(BySideStatsDto.class);
        assertThat(bySideStatsDto.getBlackSideDrawCount()).isEqualTo(1);
    }
}

package com.gitlab.akluball.chessapp.gamehistory.itest;

import com.gitlab.akluball.chessapp.gamehistory.itest.client.DirectGameRequestClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.client.GameHistoryClient;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.DirectGameRequestDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.data.GameHistoryDao;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockGameState;
import com.gitlab.akluball.chessapp.gamehistory.itest.mock.MockUser;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.DirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.ResultTag;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.TerminationTag;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.Guicy;
import com.gitlab.akluball.chessapp.gamehistory.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.GameStateSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.EndGameDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.GameStateCreateDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.UserDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.gamehistory.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.gamehistory.itest.util.Builders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;
import java.util.List;

import static com.gitlab.akluball.chessapp.gamehistory.itest.util.Util.bufferBelow4;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameHistoryIT {
    private final Builders builders;
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final DirectGameRequestDao directGameRequestDao;
    private final GameHistoryDao gameHistoryDao;
    private final MockUser mockUser;
    private final MockGameState mockGameState;
    private final UserSecurity userSecurity;
    private final GameStateSecurity gameStateSecurity;
    private final DirectGameRequestClient directGameRequestClient;
    private final GameHistoryClient gameHistoryClient;
    private final TestConfig testConfig;

    @Guicy
    GameHistoryIT(Builders builders,
            UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            DirectGameRequestDao directGameRequestDao,
            GameHistoryDao gameHistoryDao,
            MockUser mockUser,
            MockGameState mockGameState,
            UserSecurity userSecurity,
            GameStateSecurity gameStateSecurity,
            DirectGameRequestClient directGameRequestClient,
            GameHistoryClient gameHistoryClient,
            TestConfig testConfig) {
        this.builders = builders;
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.directGameRequestDao = directGameRequestDao;
        this.gameHistoryDao = gameHistoryDao;
        this.mockUser = mockUser;
        this.mockGameState = mockGameState;
        this.userSecurity = userSecurity;
        this.gameStateSecurity = gameStateSecurity;
        this.directGameRequestClient = directGameRequestClient;
        this.gameHistoryClient = gameHistoryClient;
        this.testConfig = testConfig;
    }

    private GameHistory createPersistedGameHistory() {
        GameHistory gameHistory = this.uniqueBuilders.gameHistory().build();
        this.gameHistoryDao.persist(gameHistory);
        return gameHistory;
    }

    @Test
    void requestAcceptPersists() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        List<GameHistory> gamesBetween = this.gameHistoryDao.findInvolving(requesterId, requesteeId);
        assertThat(gamesBetween).hasSize(1);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void requestAcceptNoPersistOnPutGameStateFail() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPutFail(createDto);
        this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        List<GameHistory> gamesBetween = this.gameHistoryDao.findInvolving(requesterId, requesteeId);
        assertThat(gamesBetween).hasSize(0);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void requestAcceptSetsWhiteSideUserId() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        GameHistory gameHistory = this.gameHistoryDao.findInvolving(requesterId, requesteeId).get(0);
        assertThat(gameHistory.getWhiteSideUserId()).isEqualTo(requesteeId);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void requestAcceptSetsBlackSideUserId() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        GameHistory gameHistory = this.gameHistoryDao.findInvolving(requesterId, requesteeId).get(0);
        assertThat(gameHistory.getBlackSideUserId()).isEqualTo(requesterId);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    void requestAcceptSetsMillisPerPlayer() {
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(60_000L).build();
        this.directGameRequestDao.persist(directRequest);
        this.mockUser.expectGet(requesterId);
        this.mockUser.expectGet(requesteeId);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        this.directGameRequestClient.accept(this.userSecurity.tokenFor(requesteeId), directRequest);
        GameHistory gameHistory = this.gameHistoryDao.findInvolving(requesterId, requesteeId).get(0);
        assertThat(gameHistory.getMillisPerPlayer()).isEqualTo(60_000L);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requesterId);
        this.mockUser.clearGet(requesteeId);
    }

    @Test
    @Guicy
    void equalRatingsAdjustments() {
        int kFactor = this.testConfig.eloRatingKFactor();
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directGameRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directGameRequest);
        UserDto requester = this.builders.userDto().id(requesterId).rating(1000).build();
        UserDto requestee = this.builders.userDto().id(requesteeId).rating(1000).build();
        this.mockUser.expectGet(requester);
        this.mockUser.expectGet(requestee);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        String token = this.userSecurity.tokenFor(requesteeId);
        this.directGameRequestClient.accept(token, directGameRequest);
        GameHistory gameHistory = this.gameHistoryDao.findInvolving(requesterId, requesteeId).get(0);
        assertThat(gameHistory.getWhiteWinsRatingAdjustment()).isEqualTo(kFactor / 2);
        assertThat(gameHistory.getWhiteDrawsRatingAdjustment()).isEqualTo(0);
        assertThat(gameHistory.getWhiteLosesRatingAdjustment()).isEqualTo(kFactor / -2);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requester);
        this.mockUser.clearGet(requestee);
    }

    @Test
    @Guicy
    void ratingSeparation191() {
        int kFactor = this.testConfig.eloRatingKFactor();
        int requesterId = this.uniqueData.userId();
        int requesteeId = this.uniqueData.userId();
        DirectGameRequest directGameRequest = this.builders.directGameRequest().from(requesterId).to(requesteeId)
                .millisPerPlayer(600_000L).build();
        this.directGameRequestDao.persist(directGameRequest);
        UserDto requester = this.builders.userDto().id(requesterId).rating(1191).build();
        UserDto requestee = this.builders.userDto().id(requesteeId).rating(1000).build();
        this.mockUser.expectGet(requester);
        this.mockUser.expectGet(requestee);
        GameStateCreateDto createDto = this.builders.gameStateCreateDto().whiteSideUserId(requesteeId)
                .blackSideUserId(requesterId).build();
        this.mockGameState.expectPut(createDto);
        String token = this.userSecurity.tokenFor(requesteeId);
        this.directGameRequestClient.accept(token, directGameRequest);
        GameHistory gameHistory = this.gameHistoryDao.findInvolving(requesterId, requesteeId).get(0);
        assertThat(gameHistory.getWhiteWinsRatingAdjustment()).isEqualTo(kFactor * 3 / 4);
        assertThat(gameHistory.getWhiteDrawsRatingAdjustment()).isEqualTo(kFactor / 4);
        assertThat(gameHistory.getWhiteLosesRatingAdjustment()).isEqualTo(kFactor / -4);
        this.mockGameState.clearPut(createDto);
        this.mockUser.clearGet(requester);
        this.mockUser.clearGet(requestee);
    }

    @Test
    void endGameUpdatesTags() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto().resultTag(ResultTag.BLACK_WINS)
                .terminationTag(TerminationTag.NORMAL).build();
        this.mockUser.expectEndGamePost(gameHistory);
        this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        this.gameHistoryDao.refresh(gameHistory);
        assertThat(gameHistory.getResultTag()).isEqualTo(ResultTag.BLACK_WINS);
        assertThat(gameHistory.getTerminationTag()).isEqualTo(TerminationTag.NORMAL);
        this.mockUser.clearEndGamePost(gameHistory);
    }

    @Test
    void endGameSetsMoveSummaries() {
        GameHistory gameHistory = this.createPersistedGameHistory();
        EndGameDto endGameDto = this.builders.endGameDto().resultTag(ResultTag.BLACK_WINS)
                .terminationTag(TerminationTag.TIME_FORFEIT)
                .moveSummary(this.builders.moveSummary().build()).build();
        mockUser.expectEndGamePost(gameHistory);
        this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        this.gameHistoryDao.refresh(gameHistory);
        assertThat(gameHistory.getMoveSummaries()).hasSize(1);
        mockUser.clearEndGamePost(gameHistory);
    }

    @Test
    @Guicy
    void endGameSetsEndEpochSeconds(MockUser mockUser) {
        GameHistory gameHistory = this.createPersistedGameHistory();
        MoveSummary moveSummary = this.builders.moveSummary().build();
        EndGameDto endGameDto = builders.endGameDto()
                .resultTag(ResultTag.BLACK_WINS)
                .terminationTag(TerminationTag.NORMAL)
                .moveSummary(moveSummary).build();
        mockUser.expectEndGamePost(gameHistory);
        this.gameHistoryClient.endGame(this.gameStateSecurity.getToken(), gameHistory, endGameDto);
        this.gameHistoryDao.refresh(gameHistory);
        assertThat(gameHistory.getEndEpochSeconds()).isIn(bufferBelow4(Instant.now().getEpochSecond()));
        mockUser.clearEndGamePost(gameHistory);
    }
}

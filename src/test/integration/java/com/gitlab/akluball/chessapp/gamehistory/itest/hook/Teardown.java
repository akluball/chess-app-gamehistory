package com.gitlab.akluball.chessapp.gamehistory.itest.hook;

import java.io.IOException;

import static com.gitlab.akluball.chessapp.gamehistory.itest.hook.DeployUtil.*;

public class Teardown {
    public static void main(String[] args) throws IOException, InterruptedException {
        containerAsadmin("undeploy", GAMEHISTORY_DEPLOY_NAME);
        new ProcessBuilder("docker", "stop", PAYARA_CONTAINER)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "stop", GAMESTATE_CONTAINER)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "stop", USER_CONTAINER)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "stop", DB_CONTAINER)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "network", "rm", NETWORK_NAME)
                .inheritIO().start().waitFor();
    }
}

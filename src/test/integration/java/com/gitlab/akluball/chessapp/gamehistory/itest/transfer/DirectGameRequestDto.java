package com.gitlab.akluball.chessapp.gamehistory.itest.transfer;

public class DirectGameRequestDto {
    private int id;
    private int requesterId;
    private int requesteeId;
    private long epochSeconds;

    public int getId() {
        return id;
    }

    public int getRequesterId() {
        return requesterId;
    }

    public int getRequesteeId() {
        return requesteeId;
    }

    public long getEpochSeconds() {
        return epochSeconds;
    }
}

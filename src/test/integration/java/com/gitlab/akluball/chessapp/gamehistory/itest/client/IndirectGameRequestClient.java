package com.gitlab.akluball.chessapp.gamehistory.itest.client;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.IndirectGameRequest;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static com.gitlab.akluball.chessapp.gamehistory.itest.security.SecurityUtil.bearer;

public class IndirectGameRequestClient {
    private final WebTarget target;

    @Inject
    IndirectGameRequestClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target("gamerequest/").path("indirect");
    }

    public Response create(String token) {
        Invocation.Builder invocationBuilder = this.target.request();
        bearer(invocationBuilder, token);
        return invocationBuilder.post(null);
    }

    public Response getOutgoing(String token) {
        Invocation.Builder invocationBuilder = this.target.request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response cancel(String token, String indirectGameRequestId) {
        Invocation.Builder invocationBuilder = this.target.path(indirectGameRequestId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.delete();
    }

    public Response cancel(String token, IndirectGameRequest indirectGameRequest) {
        return this.cancel(token, "" + indirectGameRequest.getId());
    }
}

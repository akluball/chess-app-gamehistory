package com.gitlab.akluball.chessapp.gamehistory.itest.transfer;

public class UserDto {
    private Integer id;
    private Integer rating;

    public Integer getId() {
        return id;
    }

    public static class Builder {
        private final UserDto userDto;

        Builder() {
            this.userDto = new UserDto();
        }

        public Builder id(Integer id) {
            this.userDto.id = id;
            return this;
        }

        public Builder rating(Integer rating) {
            this.userDto.rating = rating;
            return this;
        }

        public UserDto build() {
            return this.userDto;
        }
    }
}

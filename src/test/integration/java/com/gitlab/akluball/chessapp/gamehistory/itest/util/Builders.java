package com.gitlab.akluball.chessapp.gamehistory.itest.util;

import com.gitlab.akluball.chessapp.gamehistory.itest.model.DirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.GameHistory;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.IndirectGameRequest;
import com.gitlab.akluball.chessapp.gamehistory.itest.model.MoveSummary;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.EndGameDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.GameStateCreateDto;
import com.gitlab.akluball.chessapp.gamehistory.itest.transfer.UserDto;
import com.google.inject.Inject;

import javax.inject.Provider;

public class Builders {
    private final Provider<GameHistory.Builder> gameHistoryBuilderProvider;
    private final Provider<MoveSummary.Builder> moveSummaryBuilderProvider;
    private final Provider<EndGameDto.Builder> tagsDtoBuilderProvider;
    private final Provider<UserDto.Builder> userDtoBuilderProvider;
    private final Provider<DirectGameRequest.Builder> directGameRequestBuilderProvider;
    private final Provider<IndirectGameRequest.Builder> indirectGameRequestBuilderProvider;
    private final Provider<GameStateCreateDto.Builder> endGameDtoBuilderProvider;

    @Inject
    Builders(Provider<GameHistory.Builder> gameHistoryBuilderProvider,
            Provider<MoveSummary.Builder> moveSummaryBuilderProvider,
            Provider<EndGameDto.Builder> tagsDtoBuilderProvider,
            Provider<UserDto.Builder> userDtoBuilderProvider,
            Provider<DirectGameRequest.Builder> directGameRequestBuilderProvider,
            Provider<IndirectGameRequest.Builder> indirectGameRequestBuilderProvider,
            Provider<GameStateCreateDto.Builder> endGameDtoBuilderProvider) {
        this.gameHistoryBuilderProvider = gameHistoryBuilderProvider;
        this.moveSummaryBuilderProvider = moveSummaryBuilderProvider;
        this.tagsDtoBuilderProvider = tagsDtoBuilderProvider;
        this.userDtoBuilderProvider = userDtoBuilderProvider;
        this.directGameRequestBuilderProvider = directGameRequestBuilderProvider;
        this.indirectGameRequestBuilderProvider = indirectGameRequestBuilderProvider;
        this.endGameDtoBuilderProvider = endGameDtoBuilderProvider;
    }

    public GameHistory.Builder gameHistory() {
        return this.gameHistoryBuilderProvider.get();
    }

    public MoveSummary.Builder moveSummary() {
        return this.moveSummaryBuilderProvider.get();
    }

    public EndGameDto.Builder endGameDto() {
        return this.tagsDtoBuilderProvider.get();
    }

    public UserDto.Builder userDto() {
        return this.userDtoBuilderProvider.get();
    }

    public DirectGameRequest.Builder directGameRequest() {
        return this.directGameRequestBuilderProvider.get();
    }

    public IndirectGameRequest.Builder indirectGameRequest() {
        return this.indirectGameRequestBuilderProvider.get();
    }

    public GameStateCreateDto.Builder gameStateCreateDto() {
        return this.endGameDtoBuilderProvider.get();
    }
}

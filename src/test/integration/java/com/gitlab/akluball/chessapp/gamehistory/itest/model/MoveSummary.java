package com.gitlab.akluball.chessapp.gamehistory.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
@Access(AccessType.FIELD)
public class MoveSummary {
    private String pieceLetter;
    private String promotionPieceLetter;
    private String source;
    private String target;
    private boolean isCapture;
    @Enumerated(EnumType.STRING)
    private SourceDisambiguation sourceDisambiguation;
    private long durationMillis;
    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    public static class Builder {
        private final MoveSummary moveSummary;

        Builder() {
            this.moveSummary = new MoveSummary();
        }

        public Builder pieceLetter(String pieceLetter) {
            this.moveSummary.pieceLetter = pieceLetter;
            return this;
        }

        public Builder promotionPieceLetter(String pieceLetter) {
            this.moveSummary.promotionPieceLetter = pieceLetter;
            return this;
        }

        public Builder source(String source) {
            this.moveSummary.source = source;
            return this;
        }

        public Builder target(String target) {
            this.moveSummary.target = target;
            return this;
        }

        public Builder isCapture(boolean isCapture) {
            this.moveSummary.isCapture = isCapture;
            return this;
        }

        public Builder sourceDisambiguation(SourceDisambiguation sourceDisambiguation) {
            this.moveSummary.sourceDisambiguation = sourceDisambiguation;
            return this;
        }

        public Builder durationMillis(long durationMillis) {
            this.moveSummary.durationMillis = durationMillis;
            return this;
        }

        public Builder gameStatus(GameStatus gameStatus) {
            this.moveSummary.gameStatus = gameStatus;
            return this;
        }

        public MoveSummary build() {
            return this.moveSummary;
        }
    }
}
